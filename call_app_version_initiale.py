import os 

import dialogflow

credential_path = yourpath
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credential_path
def detect_intent_texts(project_id, session_id, texts, language_code):
    """Returns the result of detect intent with texts as inputs.

    Using the same `session_id` between requests allows continuation
    of the conversation."""

    import dialogflow_v2 as dialogflow
    session_client = dialogflow.SessionsClient()

    session = session_client.session_path(project_id, session_id)


    for text in texts:
        text_input = dialogflow.types.TextInput(
            text=text, language_code=language_code)

        query_input = dialogflow.types.QueryInput(text=text_input)
        response = session_client.detect_intent(
            session=session, query_input=query_input)
        var=response.query_result.intent.display_name


        return response.query_result.fulfillment_text
        
        



from flask import Flask, request
import time
from twilio.twiml.voice_response import VoiceResponse, Gather, Say
from twilio.twiml.messaging_response import MessagingResponse, Message
#"http://bcf0d256.ngrok.io/gather"
app = Flask(__name__)
answer="Oui bonjour, vous m'entendez?"
@app.route('/answer', methods=['GET', 'POST'])
def answer_call():
    resp = VoiceResponse()
    gather = Gather(input="speech", language="fr-FR", timeout="3", action="/gather")
    gather.say(answer,language='fr-FR')
    resp.append(gather)
    resp.redirect('/answer')
    return str(resp)

@app.route('/gather', methods=['GET', 'POST'])
def gather():
    resp = VoiceResponse() 
    texts = request.form['SpeechResult']
    print(texts)
    global answer
    answer = detect_intent_texts("essec-241318","session_test" ,[texts], "French — fr")
    print(answer)
    resp.redirect('/answer')
    return str(resp)

if __name__ == "__main__":
    app.run()