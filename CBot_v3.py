###############################################################################
################################### IMPORTS ###################################
###############################################################################

import win32com.client
from pygame import mixer
import speech_recognition as sr  
from os import system
import sys
# import os
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import datetime
#from datetime import date
import pymongo
import pandas as pd

import time
import sched
#from twilio.rest import Client
#import calendar
#import schedule
import timestring #converts string to datetime (english only)
from translate import Translator #translates (so we can use timestring in french)
#from datetime import timedelta#, datetime
#import GBot_GA.py as ga
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

import rfc3339      # for date object -> date string
import iso8601      # for date string -> date object

def get_date_object(date_string):
    return iso8601.parse_date(date_string)
def get_date_string(date_object):
    return rfc3339.rfc3339(date_object)



'''
# use creds to create a client to interact with the Google Drive API
scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', scope)
client = gspread.authorize(creds)
sheet_candidats_CDG = client.open("Dataset - Voicebot").sheet1
# Micro
speaker_CDG = win32com.client.Dispatch("SAPI.SpVoice")
'''

###############################################################################
################################### VARIABLES #################################
###############################################################################
# NE PAS TOUCHER
translator=Translator(to_lang="en",from_lang='fr')
scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive','https://www.googleapis.com/auth/calendar']
# Your Account Sid and Auth Token from twilio.com/console
# DANGER! This is insecure. See http://twil.io/secure
#account_sid = " var"
#auth_token = " var"
#client = Client(account_sid, auth_token)

# Il est risqué de modifier le dictionnaire ci-dessous, attention de bien faire matcher les cle chiffrées de GS avec lui

#le client peut ajouter plus de questions dans ce dico s'il ajoute une "cle" chiffrée dans la googlesheet (GS)
q_ref = {("00","rep-gr"):"",
("01","statut_14"):"Quel est votre statut actuel Veuillez choisir entre: CDI , CDD , Freelance , Stagiaire , Alternant",
("02","secteur"):"Dans quel secteur évoluez-vous?",
("03","annee_exp"):"Quel est votre nombre d’année d’expérience ?",
("04","poste"):"Quel poste occupez-vous actuellement ?",
("05","correspond_06"):"Est-ce que votre poste actuel vous correspond ?, repondez par oui ou non",
("06","poste_souhaite_c"):"Sur quel type de poste souhaiteriez-vous évoluer?",
("07","competences"):"Veuillez citer 3 de vos compétences. Par exemple: Python, Gestion de projet, Organisé",
("08","structure"):"Dans quel type de structure préférez-vous travailler ? Veuillez choisir entre: Petite,  Grande, ou Indifférent",
("09","ville"):"Dans quelle ville travaillez vous?",
("10","lieu"):"Quelles sont vos préférences concernant le lieu de travail ? Veuillez choisir entre: Sur site , à distance, ou Indifférent",
("11","mobile"):"Etes-vous mobile ? Veuillez choisir entre: Ville, Pays, ou International",
("12","rech_active_13"):"Etes vous en recherche active ou bien à l’écoute du marché ou pas du tout en recherche ?",
("13","disponibilite"):"Quand êtes-vous disponible ?",
("14","pretentions_c"):"Quelles sont vos prétentions salariales ?",
("15","TJM_c"):"Quel est votre taux journalier moyen ?"
}

d_mois = {"01":"janvier", "02":"fevrier", "03":"mars", "04":"avril", "05":"mai", "06":"juin", "07":"juillet", "08":"aout", "09":"septembre", "10":"octobre", "11":"novembre", "12":"decembre"}
d_jour = {"01":"premier", "02":"deux", "03":"trois", "04":"quatre", "05":"cinq", "06":"six", "07":"sept", "08":"huit", "09":"neuf", "10":"dix", "11":"onze", "12":"douze", "13":"treize", "14":"quatorze", "15":"quinze", "16":"seize", "17":"Dix-sept", "18":"Dix-huit", "19":"Dix-neuf", "20":"vingt", "21":"vingt et un", "22":"vingt deux", "23":"vingt trois", "24":"vingt quatre", "25":"vingt cinq", "26":"vingt six", "27":"vingt sept", "28":"vingt huit", "29":"vingt neuf", "30":"trente", "31":"trente et un"}
d_weekday = {"Monday":"lundi", "Tuesday":"mardi", "Wednesday":"mercredi", "Thursday":"jeudi", "Friday":"vendredi", "Saturday":"samedi", "Sunday":"dimanche"}

#%%
'''
dico_ref = {'00':'rep-gr','01':'statut_14','02':'secteur','03':'annee_exp','04':'poste','05':'correspond_06',
            '06':'poste_souhaite_c','07':'competences','08':'structure','09':'ville','10':'lieu',
            '11':'mobile','12':'rech_active_13','13':'disponibilite','14':'pretentions_c','15':'TJM_c'}

l_dico_ref = []
for i in dico_ref.keys():
    l_dico_ref.append(i)
lcles0 = []
for i in from_gs:lcles0.append(i[:2])
for s in lcles0:
    if s not in l_dico_ref[0]:print("Nouvelle valeur dans la googlesheet non référencée, complétez le dico 'dico_ref' ci-dessus")
lcles1 = []
for i in lcles0:
    lcles1.append(q_ref.get(i))
lvals = []
for i in lcles1:
    lvals.append(q_ref.get(i))
dico_questions = {}
if len(lcles0) == len(lcles1) == len(lvals):
    for i in range(len(lcles0)):
        dico_questions.update({(lcles0[i],lcles1[i]):lvals[i]})
else:print('len different between the 3 lists above')
index_00 +=1
'''
#%%
###############################################################################
################################### FONCTIONS #################################
###############################################################################

########### CONNECTIONS

def connect_to_GA(scope):
    """Shows basic usage of the Google Calendar API.
    Prints the start and name of the next 10 events on the user's calendar.
    """
    global service_CDG
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'): # cela permet de checker si le fichier pickle n'a pas été déjà créé
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request()) 
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', scope)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token: # cela permet de recréer le fichier pickle
            pickle.dump(creds, token)

    service_CDG = build('calendar', 'v3', credentials=creds)

def d_connect_to_mongo(path='mongodb://localhost:27017/', bdd_name="BCGD", bdd_collec="VBot_Vivier"):
    '''
    Used by main, No sub
    Insere un dico dans une base MongoDB
    params :
        STRING : le chemin de la base, generalement : 'mongodb://localhost:27017/'
        STRING : le nom de la base, ici : "BCGD"
        STRING : le nom de la collection, ici : "VBot_Vivier"
        DICT : le dictionnaire a inserer
    '''
    # Creation de la database, collection, insertion
    # A noter qu'une db mongo ne sera pas créée tant qu'elle ne contient pas un 'document'
    global mydb_CDG
    global candidats_collect_CDG
    conn_mdb_str = path
    myclient = pymongo.MongoClient(conn_mdb_str) # creation d'une instance mongo
    mydb_CDG = myclient[bdd_name] # creation de la db
    candidats_collect_CDG = mydb_CDG[bdd_collec] # creation de la collection
    
def Connections(scope, sp_rate=0.9, sp_volume=100):
    '''
    Used by main, No sub
    Lance toutes les connections aux divers services
    params:
        FLOAT : vitesse de la parole de l'ordi
        INT : volume de la parole de l'ordi
    return : 
        objet sheet de google sheet
        objet speaker_CDG du micro
    '''
    global sheet_candidats_CDG
    global speaker_CDG
    creds = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', scope)
    client = gspread.authorize(creds)
    try:
        sheet_candidats_CDG = client.open("Dataset - Voicebot").sheet1
        print("connection etablie avec la GS")
    except:
        sys.exit("Désolé, programme arreté car pas de connection avec la GS")
    try:
        connect_to_GA(scope)
        print("connection etablie avec GA")
    except:
        sys.exit("Désolé, programme arreté car pas de connection avec GA")
    try:
        d_connect_to_mongo()
        print("connection etablie avec MongoDB")
    except:
        sys.exit("Désolé, programme arreté car pas de connection avec MongoDB")
    speaker_CDG = win32com.client.Dispatch("SAPI.SpVoice")
    speaker_CDG.Rate = sp_rate
    speaker_CDG.Volume = sp_volume

#### VARIABLES de CONNECTION et GLOBAL ####
Connections(scope)
global sheet_candidats_CDG
global service_CDG
global mydb_CDG
global candidats_collect_CDG
global speaker_CDG

global infos_candidat
global entete_used
global pc_rdv
global non_rdv

########### FIN CONNECTIONS

########### RDV

" this function take parameter date"    
def prise_rdv(date, scope):
    global service_CDG
    #connect_to_GA(scope)
    global infos_candidat
    global entete_used
    name = infos_candidat.get(entete_used.get("Nom"))
    tel = infos_candidat.get(entete_used.get("Tel"))
    mail = infos_candidat.get(entete_used.get("Mail"))
    date=str(date).replace(" ","T")+"+02:00"
    print(date)
    test_date = get_date_object(date) 
    test_date=test_date + datetime.timedelta(minutes=12)# ajoute 15 minutes à l'input de la date
    test_string = get_date_string(test_date)# retransforme en date internet
    print('Getting the upcoming event')
    events_result = service_CDG.events().list(calendarId='primary', timeMin=date,
                                        maxResults=1, singleEvents=True,
                                        orderBy='startTime').execute()
    events = events_result.get('items', [])
    # Call the Calendar API

    if not events:
        print('No upcoming events found.')
    for event in events:
        start = event['start'].get('dateTime', event['start'].get('date')) #affiche l'event qui viendra à posterieuri de la date donnée
        '''print('upcoming event')
        print(start, event['summary'])
        print(start[0:16])

    print("scheduling time")
    print(date[0:16])
    print("event time")
    print(start[0:16])'''

    if start[0:16]!=date[0:16]: #si l'evenement d'après est le même que celui donné, alors on ne créé pas l'event
        evenoto = {
  'summary': name,
  'location': '',
  'description': tel,
  'start': {
    'dateTime': date,
    'timeZone': 'Europe/Paris',
  },
  'end': {
    'dateTime': test_string,
    'timeZone': 'Europe/Paris',
  },
  'recurrence': [
    'RRULE:FREQ=DAILY;COUNT=1'
  ],
  'attendees': [
    {'email': mail},
  ],
  'reminders': {
    'useDefault': False,
    'overrides': [
      {'method': 'email', 'minutes': 24 * 60},
      {'method': 'popup', 'minutes': 10},
    ],
  },
}
        event = service_CDG.events().insert(calendarId='primary', body=evenoto).execute()
        print('Event created: %s' % (event.get('htmlLink')))
        var = "empty"
    else:
        print("This slot is already taken")
        var = "pas empty"
    return var       

########### FIN RDV
    
########### SPEAK IA

def tospeak(text):
    '''
    Used by main, Sub
    permet a l'ordi de parler
    params : STRING : ce que vous voulez que l'ordi dise
    '''
    global speaker_CDG
    speaker_CDG.Speak(text)

def play_mp3(path, fic):
    """
    Non utilisé pour le moment
    Play un fichier son mp3
    params :
        STRING : path : chemin absolu ou se trouve le fichier mp3
        STRING : fic :  nom du fichier audio, pas besoin de l'extension
    """
    if "." in fic:
        fic = fic.split(".", 1)[0]
    fic = path + "/" + fic + ".mp3"
    mixer.init()
    mixer.music.load(fic)
    mixer.music.play()
    
########### FIN SPEAK IA
    
########### Questionnaire interaction

def create_dico_q():
    '''
    Used by main, No sub
    Creation du dico (final) des questions a poser, A lancer a chaque modification des en-tete de colonne dans la googlesheet
    return :
        DICT : dico contenant les questions que l'on va poser
        INT : numero de colonne, dans la GS, ou se trouve '00' (permet l'insertion dans la GS au bon endroit)
    '''
    global sheet_candidats_CDG
    from_gs = sheet_candidats_CDG.row_values(1)
    index_00 = from_gs.index([i for i in from_gs if i.startswith('00')][0])
    del from_gs[0:index_00]
    lcles0 = []
    for i in from_gs:lcles0.append(i[:2])

    dico_questions = {}
    for i in lcles0:
        temp = [v for v in q_ref.items() if v[0][0]==i]
        dico_questions.update({temp[0][0]:temp[0][1]})    
    
    return dico_questions, index_00

def decoderep(nrj_threshold=100, duree=3):
    '''
    Sub (ask_listen)
    Valide les propos (en rapport avec le questionnaire)
    params : 
        INT : qualité audio
        INT : temps de reponse
    return : la validation
    '''
    rrep = sr.Recognizer()
    with sr.Microphone() as sourcerep:
        rrep.dynamic_energy_threshold = nrj_threshold # False or 50-4000 are good parameters
        rrep.adjust_for_ambient_noise(sourcerep, duration = 1)
        tospeak('Vous confirmez ?, répondez par exactement ou faux, c\'est a vous')
        audio = rrep.record(sourcerep, duration = duree, offset = None)
        #tospeak('analyse de votre confirmation')
        rep = rrep.recognize_google(audio, language='fr-FR', show_all=False)
        tospeak('vous venez de dire '+rep) # A voir si on garde ou vire
        return rep	

def ask_listen(question, nrj_threshold = 100, timeo = 4, duree = 4): # wavefile
    '''
    Sub (ask_listen_main, get_answers)
    Prend en charge 1 question du questionnaire
    params :
        STRING : la question
        INT : divers qualité audio : threshold, timeo
        INT : duree d'enregistrement (temps laissé pour la reponse)
    return :
        STRING : la reponse
        False si reponse incomprise
    '''
    string = ""
    r = sr.Recognizer()
    with sr.Microphone(device_index=None) as source: # WavFile(wavefile)
        r.dynamic_energy_threshold = True
        r.energy_threshold = nrj_threshold # False or 50-4000 are good parameters
        r.adjust_for_ambient_noise(source, duration = 1)
        tospeak(question)
        system("{}".format(question))
        #audio = r.listen(source, timeout = timeo)
        audio = r.record(source, duration = duree, offset = 0.1)
        #tospeak('j\'analyse votre réponse')
        #system("say Analyse de votre réponse")
        try:
            string = ''.join(r.recognize_google(
                audio, language='fr-FR', show_all=False))
            tospeak('votre réponse est '+string)
            system("say votre réponse est {}".format(string))
            rep = decoderep(nrj_threshold, 3)
            if rep in ['exactement', 'exact', 'xactement', 'oui'] or rep.endswith(("men","man","ment","mand","mant")):
                tospeak('votre réponse est donc %s' % string)
                system("say votre réponse est donc {}".format(string))
                return string
            else:
                return False
        except sr.UnknownValueError:
            return False
    return string

def ask_listen_main(question, ds_nrj=100, ds_timeo=4, ds_duree=4):
    '''
    Sub (get_answers)
    Gere les incomprehensions provenant de la parole (en rapport avec le questionnaire)
    params :
        STRING : la question
        INT : divers qualité audio : threshold, timeo
        INT : duree d'enregistrement (temps laissé pour la reponse)
    return :
        True si la reponse est comprise
        False : Sort au bout de 3 echecs
    '''
    compris = False # (WAVFILE)
    j=0
    while not compris:
        compris = ask_listen(question, ds_nrj, ds_timeo, ds_duree)
        if not compris:
            if j<2:
                tospeak("N'ayant pas obtenu une réponse satisfaisante, je repose la question")
        j+=1
        if j == 3:
            tospeak("Je n'ai toujours pas compris, tant pis pour cette réponse, passons a la question suivante")
            break
    tospeak('question suivante')
    return compris

##################

def cond_ask(question_c, answers):
    '''
    Sub(get_answers)
    gere les specificites du questionnaire, les questions ayant des conditions
    param :
        STRING : Numero de la question (ex : 01 ou 14)
        DICT : dico possedant les reponses deja obtenue, l'idee etant d'avoir des conditions sur ces reponses
    return : Boolean
        True si la question concernée doit etre posée sinon False
    '''
    if question_c == "06":
        if answers.get(('05', 'correspond_06')).lower() != "non":
            return False
    elif question_c == "14":
        l1 = ["cdi", "cdd", "stagiaire", "alternant"]
        if answers.get(('01', 'statut_14')).lower() not in l1:
            return False
    elif question_c == "15":
        l2 = ["freelance", "silence", "lance", "lens"]
        if answers.get(('01', 'statut_14')).lower() not in l2:
            return False
    return True

def get_answers(question_dict):
    """
    Sub (chat)
    Gere tout le questionnaire et retourne le dico des reponses obtenues :
        returns a dico with key:name of the question, value:answer to the question
    params :
        DICT : dico contenant les questions a poser
    return :
        DICT : dico avec les reponses            
    """
    answers = {}
    del question_dict[('00', 'rep-gr')]
    for question in question_dict:
        if question[0] in ["06", "14", "15"]:
            ask = cond_ask(question[0], answers)
        else: ask = True
        if ask:
            try:
                answers.update({question:input(question_dict[question])}) #############INPUT########
            except ValueError:
                print("Je n'ai pas pu récuperer la réponse à cette question, mais tant pis je passe à la suite")
        else:
            answers.update({question:"Non applicable"})
    return answers
'''
def ask(nom_question,question,dico):
	"""asks question and puts it the answer a dico"""
	#system(question)
	answer = ask_listen_main(question)
	dico.update({nom_question:answer})
'''

########### FIN Questionnaire interaction

########### Date & RDV

def date_form_to_speak(la_date, separator_date="-"): # pas de param sur les 3 dicos, il se peut qu'ils soient necessaire
    '''
    Sub (speak_date)
    A partir d'une date sous forme string, prepare la phrase a dire concernant cette date
    !!! ATTENTION !!! No much control on that string, please follow rules in params
    param :
        STRING : la date sous forme yyyy-MM-dd ou yyyy/MM/dd
        STRING : le symbole separant les jours, mois et annee, ex : "-" ou "/"
    return :
        STRING : la phrase prete a etre parlée
    '''
    sep = separator_date
    if sep == "-":
        sep_speak = "les tirets"
    elif sep == "/":
        sep_speak = "la barre oblique"
    else:
        sep_speak = "le symbole de separation"

    if sep in la_date:
        year, month, day = (int(x) for x in la_date.split(sep))   
        rep = datetime.date(year, month, day)
        le_weekday = rep.strftime("%A")
        le_weekday = d_weekday.get(le_weekday)
        le_jour = d_jour.get(la_date.split(sep)[2])
        le_mois = d_mois.get(la_date.split(sep)[1])
        le_annee = la_date.split(sep)[0]
        if len(le_annee) != 4:
            if len(le_annee) == 2:
                le_annee = "20"+le_annee
            else:
                return "Erreur, format de l'année incorrecte"
        
        date_to_speak = le_weekday + " " + le_jour + " " + le_mois + " " + le_annee
    else:
        return "Erreur, format de la date incorrecte, manque {}".format(sep_speak)
    return date_to_speak

def speak_date(param_dt):
    '''
    Used by main, No sub
    Parle la date et l'heure qui doivent etre sous un format précis
    params :
        STRING, format : "yyyy-MM-dd?HH:mm*", sachant que ? correspond a 1 caractere et * a plusieurs (peu importe lesquels)
    '''
    la_date = param_dt[:10]
    l_heure = param_dt[11:16]
    tospeak(date_form_to_speak(la_date) + " a " + l_heure)

def special_cases(answer, dt):
    #########################Traitement de cas speciaux########################
    today = datetime.datetime.today()
    if 'prochain' in answer or dt<today:
		#the problem being that if you say 'next thursday' it will return thursday of this week, even if it's friday!!!
		#so let's try and fix this by adding 7 days 
        w = datetime.timedelta(days=7)
        dt = dt+w
    if 'soir' in answer:
        dt = dt.replace(hour=18)
    if 'midi' in answer:
        dt = dt.replace(hour=12)
    if 'weekend' in answer:
        #si qqn dit 'weekend' on met par défaut le dimanche (à la même heure?)
        today = datetime.datetime.today()
        #d = datetime.strptime('2013-05-27', '%Y-%m-%d') # Monday
        t = datetime.timedelta((13 - today.weekday()) % 7) #13 = dimanche , 12 = samedi
        dt = today + t
        #on reconverti en datetime
        dt = datetime.datetime(dt.year,dt.month,dt.day,dt.hour,dt.minute)
    if 'après demain' in answer or 'après-demain' in answer:
        x = datetime.timedelta(days=1)
        dt=dt + x
    if dt.hour == 0:
        #si on dit un jour seulement, par défaut il retourne minuit, donc on change à 15h
        #pour ne pas appeler à minuit quoi 
        dt = dt.replace(hour=15)
    return dt

def ask_time():
    #translates the answer into english and then converts it into datetime inshalla 
    #returns datetime of rdv to put in the calendar/ sheet
    #RETURNS NONE IF NO DATE FOUND
    today = datetime.datetime.today()
    p,j = False,0
	#>>>>>>BOUCLE
    while(p==False and j<3):
        answer = input("Quel jour et à quelle heure puis-je vous rappeler? : ")   ### INSERTLISTEN ask_listen_begin
        tospeak('Veuillez patienter le temps que je traite votre proposition')
        trans_ans = translator.translate(answer) #translate
        print("1", trans_ans)   ### TO_DEL
        try:
            dt = timestring.Date(trans_ans)
            #if dt < today:
             #   print('avant')
              #  tospeak('le rendez-vous ne peut pas etre une date passée')
            #else:
                #to be able to manipulate 'dt', the type must be datetime not timestring >>>
            dt = datetime.datetime(dt.year,dt.month,dt.day,dt.hour,dt.minute)#convert to datetime
            dt = special_cases(answer, dt)
            print("voici la date", dt)
            if dt >= today:
                p=True
            else:
                tospeak('le rendez-vous ne peut pas etre une date passée')
        except:
            tospeak("Désolée, je n'ai pas compris la date")
        j+=1
	#>>>>>>BOUCLE /fin 
    if p==False:
        return None #RETURNS NONE IF NO DATE FOUND = PROBABLY MEANS THE INTERLOCUTER DOESN'T WANT A RDV
    
    return dt

#main function
def time_main():
    """cette fonction demande à l'utilisateur s'il souhaite prendre rdv
    si la réponse est oui il récupère la date souhaitée, demnande la confirmation et puis le retourne
    il retourne None si l'utilisateur ne veut pas de rendez-vous ou si l'ordi ne comprends pas l'user plus que 3 fois"""
    global pc_rdv
    global non_rdv
    non_rdv = False
    pc_rdv = False
    oui = ['oui','oui oui', 'ok', 'pas de souci', 'okay',"d'accord","ouais","wesh","bon d'accord",'alrighty then', 'yes', 'oki']
    non = ['non', 'non non', "no", 'pas disponible','je suis occupé','etc', 'pas du tout','get lost', 'désolée','non désolée',"j'ai pas le temps", "pas vraiment","pas trop", "pas beacoup","absolument pas",'nope','pas envie']
    q,i = False,0
    while (q==False and i<3):
        rep = input("Seriez-vous d'accord qu'on vous rappelle à une heure qui vous convient? : ")   ### INSERTLISTEN ask_listen_begin
        if rep in oui:
            q=True
            tospeak("Très bien.")
        elif rep in non:
            non_rdv = True
            return None
        else:
            if i < 2:
                tospeak("Je n'ai pas compris votre réponse")
            i+=1
    if (i==3 and q==False):
		#print("Je suis désolée je n'ai toujours pas compris votre réponse, tant pis, je vous souhaite une bonne journée, au revoir")
        pc_rdv = True
        return None
	#he has to try 3 times to take a rdv but if the person doesn't confirm 3 times, then hang up !	
    k=0
    l=0
    while(k<3):
        date = ask_time()
        print("je sors de AT", date)   ### TO_DEL
        if date != None:
            while(l<3):
				#tospeak("Vous avez choisi le {} {} à {} heures {}".format(date.day,date.month,date.hour,date.minute))
                tospeak("Vous avez choisi le ")
                speak_date(str(date))
                r=input("confirmez vous ce rendez-vous ?")   ### INSERTLISTEN ask_listen_begin
                if r in oui:
					#tospeak("Merci, je vous rappellerai donc le {} {} à {} heures {}".format(date.day,date.month,date.hour,date.minute))
                    ga_conf_date = prise_rdv(date)
                    if ga_conf_date == 'empty':
                        return date
                    else:
                        tospeak("cette date est prise")
                        break
                elif r in non:
                    tospeak("Vous ne confirmez pas donc je vous repose la question")
                    break
                elif r not in oui and r not in non:
                    if l < 2:
                        tospeak("Désolé Je n'ai pas compris")
                l+=1
        else:
            pc_rdv = True
            return None
        if l==3:
            tospeak("Désolée, je n'arrive toujours pas à comprendre votre confirmation")
			#demander à boris quoi faire dans ce cas
            break
        k+=1
	#print("N'ayant pas pu obtenir une réponse pour la prise de rendez-vous, je vais raccrocher")
    pc_rdv = True
    return None
    
########### FIN Date & RDV

########### Chat et debut conversation

def ask_listen_begin(question, nrj_threshold = 100, timeo = 4, duree = 4):
    '''
    Sub (chat)
    Prend en charge 1 question du chat
    params :
        STRING : la question
        INT : divers qualité audio : threshold, timeo
        INT : duree d'enregistrement (temps laissé pour la reponse)
    return :
        STRING : la reponse
        None si reponse incomprise
    '''
    answer = ""
    r = sr.Recognizer()  
    with sr.Microphone(device_index=None) as source:
        r.dynamic_energy_threshold = True
        r.energy_threshold = nrj_threshold
        r.adjust_for_ambient_noise(source, duration = 1)
        tospeak(question)
        audio = r.record(source, duration = duree, offset = 0.1)
        try:
            answer = r.recognize_google(audio, language='fr-FR')
            #print("Vous avez dit : {}".format(answer))
        except sr.UnknownValueError:
            return None
    return answer

def chat(num_ligne, question_dict):
    '''
    Used by main, No sub
    gere la conversation avec l'interlocuteur, du "bonjour" au "au revoir"
    params :
        INT : numero de ligne, dans la Google Sheet (GS), de la personne appelée
        DICT : dico des questions a poser
        Obj Sheet de GS : la sheet ou se trouve :
            les personnes a appeler
            le questionnaire avec les cles chiffrées
    return :
        DICT : dico avec les réponses
    '''
    #global sheet_candidats_CDG
    #answers = {"name":name}#pour stocker les réponses
    echanges = {"greetings":"je suis Boris, un robot developpé par des étudiants pour récolter de l'information. Est-ce que vous auriez 5 minutes pour répondre à quelques questions concernant votre situation professionnel? Veuillez repondre clairement par oui ou non",
"yes":"Merci, commençons",
"no":"Cela ne prendra que 5 minutes! Voulez-vous continuer ?",
"r1":"Je n'ai pas compris votre réponse, Avez vous 5 minutes a m\'accorder ? répondez par oui ou non",
"r_non":"Je n'ai pas compris votre réponse,Cela ne prendra que 5 minutes! Voulez-vous continuer ?",
"fin_non":"J\'ai pris note de votre refus",
"fin_pc":"Je suis désolé, je n'ai toujours pas compris. Je vais raccrocher, je tenterai de vous rappeler a un autre moment. Merci",
"fin_oui":"Merci beaucoup de nous avoir accordé votre temps",
"greetings_end":"Si vous souhaitez nous rappeler par rapport à nos offres de travail, vous pouvez nous joindre au 06, je répète 06, bonne journée et bonne continuation",
"rdv_non":"J\'ai pris note de votre refus concernant le questionnaire et le rendez-vous",
"rdv_pc":"N'ayant toujours pas pu prendre un rendez-vous, je tenterai de vous rappeler a un autre moment",
"rdv_oui":"Merci"
}
    global non_rdv
    non_rdv = False
    global pc_rdv
    pc_rdv = False
    oui_rdv = False
    answers = {}
    global infos_candidat
    global entete_used
    name = infos_candidat.get(entete_used.get("Civ")) + ' ' + infos_candidat.get(entete_used.get("Nom"))
    #name = sheet_candidats_CDG.cell(num_ligne, 2).value + ' ' + sheet_candidats_CDG.cell(num_ligne, 4).value
    tospeak("Bonjour {}".format(name))
    oui = ['oui','oui oui', 'ok', 'pas de souci', 'okay','d\'accord','ouais','wesh','bon d\'accord','alrighty then', 'yes', 'pourquoi pas', 'pas de problème']
    non = ['non', 'non non','pas disponible','je suis occupé','etc', 'pas du tout','get lost', 'désolée','non désolée',"j'ai pas le temps", "pas vraiment","pas trop", "pas beacoup","absolument pas",'nope']
	#### demander a la personne si elle veut continuer ####
    rep_gr = input(echanges["greetings"])   ### INSERTLISTEN ask_listen_begin
    indice_pc = 0
    indice_non = 0
    
    while indice_pc < 3 and indice_non < 2:
           
        if rep_gr.lower() in oui:
            tospeak(echanges["yes"])
            system(echanges["yes"])
            #answers.update({('00', 'rep-gr'):"oui"})
            ####### QUESTIONNAIRE ###########
            answers.update(get_answers(question_dict.copy()))
            #################################
            tospeak(echanges["fin_oui"] + " {}".format(name))
            system(echanges["fin_oui"])   ######
            break
        
        elif rep_gr.lower() in non:
            indice_non += 1
            #system("say Cela ne prendra seulement 5 minutes! Voulez-vous continuer?")
            rep_gr = input(echanges["no"])   ### INSERTLISTEN ask_listen_begin
            if rep_gr.lower() in non:
                rdv = time_main() # Prise de rdv ?
                if rdv!=None:
                    tospeak("Je vous rappellerai donc le ")
                    speak_date(str(rdv))
                    oui_rdv = True
                break
        
        else:
            while rep_gr.lower() not in oui and rep_gr.lower() not in non or rep_gr == None:
                indice_pc += 1
                if indice_pc > 2:
                    break
                #system("say pas compris, Je vais répéter la question")
                if indice_non == 0:
                    rep_gr = input(echanges["r1"])# ask again   ### INSERTLISTEN ask_listen_begin
                else:
                    rep_gr = input(echanges["r_non"])   ### INSERTLISTEN ask_listen_begin
                    if rep_gr.lower() in non:
                        indice_non += 1
                        # rep = ask_listen_begin("seriez vous d'accord pour que je vous rappelle a un moment qui vous convient ?")
                        # if rep in oui:
                        rdv = time_main() # Prise de rdv ?
                        if rdv!=None:
                            tospeak("Je vous rappellerai donc le ")
                            speak_date(str(rdv))
                            oui_rdv = True
                        break
    # End while
    if oui_rdv:
        tospeak(echanges["rdv_oui"])
        system("say"+echanges["rdv_oui"]) ######
        answers.update({('00', 'rep-gr'):"rdv pris"})
        
    elif pc_rdv:
        tospeak(echanges["rdv_pc"])
        system("say"+echanges["rdv_pc"]) ######
        answers.update({('00', 'rep-gr'):"recall, rdv pas compris"})
        
    elif non_rdv:
        tospeak(echanges["rdv_non"])
        system("say"+echanges["rdv_non"]) ######
        answers.update({('00', 'rep-gr'):"rdv refusé"})
        
    elif rep_gr.lower() in non:
        tospeak(echanges["fin_non"])
        system("say"+echanges["fin_non"]) ######
        answers.update({('00', 'rep-gr'):"non"})   ### EN CAS DE REFUS : RETOUR NON ###
          
    elif rep_gr.lower() not in oui and rep_gr.lower() not in non:
        tospeak(echanges["fin_pc"])
        system("say"+echanges["fin_pc"]) ######
        answers.update({('00', 'rep-gr'):"recall"})   ### EN CAS D'INCOMPREHENSION : RETOUR FALSE ###

    tospeak(echanges["greetings_end"])
    return answers

########### FIN Chat et debut conversation
    
########### Google Sheet

def fill_gs(num_row, index_first_ans, dic_answers): # pas de param sheet !!! etrange !!! a voir car si pas necessaire, peut etre peut etre viré des autres def
    '''
    Used by main, No sub
    Rempli la GS (en fonction des reponses obtenue) sur la ligne de la personne appelée
    params :
        INT : numero de la ligne, dans la GS, de la personne concernée
        INT : numero de colonne, dans la GS, ou on commence a ecrire (en theorie cela devrait etre la colonne avec '00')
        DICT : dictionnaire des reponses obtenues
    '''
    global sheet_candidats_CDG
    index_first_ans += 1
    if (('00', 'rep-gr') in dic_answers):
        sheet_candidats_CDG.update_cell(num_row, index_first_ans, dic_answers.get(('00', 'rep-gr')))
    else:
        sheet_candidats_CDG.update_cell(num_row, index_first_ans, "oui")
        for i in range(len(dic_answers)):
            sheet_candidats_CDG.update_cell(num_row, i+index_first_ans+1, list(dic_answers.values())[i])
    sheet_candidats_CDG.update_cell(num_row, 1, str(datetime.datetime.now()))
    
def delete_gs_fill_answers(num_row, index_first_ans, dic_answers):
    '''
    Used by main, No sub
    Delete les reponses (la ligne), dans la GS, de la personne concernée
    !!! ATTENTION !!! si une modification des colonnes a été faite dans la GS, cette fonction risque d'effacer des données autres que les reponses
    params :
        INT : numero de la ligne, dans la GS, de la personne concernée
        INT : numero de colonne, dans la GS, ou on commence a effacer (en theorie cela devrait etre la colonne avec '00')
        DICT : dictionnaire des reponses obtenues
    '''
    global sheet_candidats_CDG
    index_first_ans += 1
    for i in range(len(dic_answers)+1):
        sheet_candidats_CDG.update_cell(num_row, i+index_first_ans, "")
    sheet_candidats_CDG.update_cell(num_row, 1, "")
    
def get_num_col(entete_col):
    '''
    retourne un numero de colonne a partir de son en-tete
    params :
        STRING : Intitulé de la cellule en tete de la colonne a chercher
    return :
        le numero de colonne si trouvée sinon 999
    '''
    global sheet_candidats_CDG
    j=0
    for i in sheet_candidats_CDG.row_values(1):
        if i == entete_col:
            return j+1
        j+=1
    return 999

def get_num_row(num_col, data):
    '''
    retourne un numero de ligne dans la GS a partir du numero de colonne et de la data que l'on cherche
    Il peut etre utile d'utiliser la fonction "get_num_col" pour trouver le bon numero de colonne a partir
    de son en-tete de colonne (qui se trouve a la ligne 1)
    params :
        INT : Numero de colonne de la colonne dans laquelle on doit chercher la data
        STRING : data a chercher
    return:
        INT : numero de ligne ou se trouve la data
    '''
    global sheet_candidats_CDG
    try:
        num_row = sheet_candidats_CDG.col_values(num_col).index(data)
    except ValueError:
        sys.exit("Cette donnée n'existe pas ou plus ({})\nveuillez rentrer une donnée correct".format(data))
    return num_row+1

def get_the_num_row(entete_col, data):
    '''
    retourne un numero de ligne dans la GS a partir d'un en-tete de colonne et de la data que l'on cherche
    params :
        STRING : Intitulé de la cellule en tete de la colonne ou se trouve la data que l'on cherche
        STRING : data a chercher
    return:
        INT : numero de ligne ou se trouve la data
    '''
    try:
        num_row = get_num_row(get_num_col(entete_col), data)
    except ValueError:
        sys.exit("Cette donnée n'existe pas dans cette colonne\nveuillez rentrer des données correctes")
    return num_row

########### FIN Google Sheet
    

    
########### MongoDB

def d_answer_to_mongo(dic):
    global candidats_collect_CDG
    # Pour insérer un dico
    candidats_collect_CDG.insert_one(dic)

def delete_mongo_collec(bdd_collec):
    '''
    Used by main, No sub
    Vide la collection dans une base MongoDB
    params :
        STRING : le nom de la collection, ici : "VBot_Vivier"
    '''
    global mydb_CDG
    # Suppression d'une collection (des data de la table)
    mydb_CDG.drop_collection(bdd_collec)
    # Suppression de la db
    #myclient.drop_database('BCGD')

def mongo_to_df(bdd_collec="VBot_Vivier"):
    '''
    Used by main, No sub
    Recupere la collection d'une base MongoDB dans un DF
    params :
        STRING : le nom de la collection, ici : "VBot_Vivier"
    return :
        DF : dataframe contenant les infos qui se trouvaient dans la collection de la base Mongo
    '''
    global candidats_collect_CDG
    # Pour voir les documents dans un dataframe
    #data = mydb_CDG.data
    df = pd.DataFrame(list(candidats_collect_CDG.find()))
    return df

def one_candidat_to_mongo(tel, bdd_collec="VBot_Vivier"):
    '''
    rentre les données d'un candidat dans mongo a partir de son numero de telephone (voir format)
    params :
        STRING : le nom de la collection, ici : "VBot_Vivier"
        STRING : tel format : "00 00 00 00 00"
    '''
    global sheet_candidats_CDG
    global mydb_CDG
    global candidats_collect_CDG
    #candidats_collect_CDG = mydb_CDG[bdd_collec]
    candidats = sheet_candidats_CDG.col_values(5)
    tel_camecol = ["tel", "tél"]
    if [i for i in tel_camecol if i in candidats[0].lower()] == []:
        raise Exception("Error: la colonne téléphone a été déplacée ou est mal nommée")
    if tel not in candidats:
        tel = format_tel(tel)
    if tel not in candidats:
        raise Exception("Error: Numero de telephone introuvable, respecter le format 00 00 00 00 00")
    else:
        d = {}
        cles = sheet_candidats_CDG.row_values(1)
        vals = sheet_candidats_CDG.row_values(candidats.index(tel)+1)
        for i in range(len(vals)):
            d.update({cles[i]:vals[i]})
        candidats_collect_CDG.insert_one(d)


def GS_to_mongo(bdd_collec="VBot_VivierfromGS"):
    '''
    Used by main, No sub
    Pour inserer les infos de la GS dans la base mongo - Sauvegarde de la GS
    params :
        STRING : le nom de la collection, ici : "VBot_VivierfromGS"
        DICT : le dictionnaire a inserer
    '''
    global sheet_candidats_CDG
    #global mydb_CDG
    global candidats_collect_CDG
    # Creation de la database, collection, insertion
    # A noter qu'une db mongo ne sera pas créée tant qu'elle ne contient pas un 'document'
    #candidats_collect_CDG = mydb_CDG[bdd_collec]
    gs_data = sheet_candidats_CDG.get_all_records()
    for d in gs_data:
        candidats_collect_CDG.insert_one(d)

########### FIN MongoDB
        
########### PREPARATION A LA CONVERSATION
        
def format_tel(tel):
    '''
    Convertit le numero de telephone en un format qui convient : "00 00 00 00 00"
    params :
        STRING : le numero de telephone a convertir
    return :
        STRING : le numero de telephone formaté
    '''
    tel = "0" + tel[-9:]
    new_tel = tel[:2] + " " + tel[2:4] + " " + tel[4:6] + " " + tel[6:8] + " " + tel[8:10]
    return new_tel
    
def main_conv(tel):
    '''
    Gere le progrmme d'appel a un candidat
    Params:
        tel : STRING : Numero de telephone de la personne a appeler
    '''
    global sheet_candidats_CDG
    if len(tel) != 14 or not tel.startswith("0"):
        tel = format_tel(tel)
    global infos_candidat
    global entete_used
    entete_used = {"Civ":"Civilité","Prenom":"Prénom", "Nom":"Nom","Tel":"Téléphone","Mail":"Mail"}
    # On lance les connections necessaires
    Connections(scope, sp_rate=0.9, sp_volume=100)
    # On se connecte a mongo avec les bons parametrages
    d_connect_to_mongo(path='mongodb://localhost:27017/', bdd_name="BCGD", bdd_collec="VBot_Vivier")
    # Si besoin pour mp3
    #pathformp3 = "C:/Formation/Simplon-Dev_Data_IA/ML/Projet_Fin/Audio"
    # On recupere le nb de lignes max de personnes ecrites dans la GS
    max_num_ligne = len(sheet_candidats_CDG.col_values(2))
    # On crée le dico des questions que l'on va poser et qui est basé sur les colonnes de la GS
    dico_questions, index_00 = create_dico_q()
    # On initialise le dico qui va contenir nos reponses
    ans = {}
    # Saisir le numero de ligne, dans la GS, de la personne a appeler
    num_ligne = get_the_num_row(entete_used.get("Tel"), tel)
    #num_ligne = input("Saisissez un numero de ligne supérieur a 1 :\n")
    try:
        num_ligne = int(num_ligne)
        assert num_ligne > 1
        assert num_ligne < max_num_ligne+1
        infos_candidat = {x:y for x,y in zip(sheet_candidats_CDG.row_values(1), sheet_candidats_CDG.row_values(num_ligne))}
        for s in entete_used.values():
            conf = infos_candidat.get(s)
            if conf == None:
                print("Vous avez modifié l'entete de colonne correspondant a {}".format (s))
                sys.exit("réécrivez le ou les en-tete corrects dans la Google Sheet ou modifier la valeur dans le programme (entete_used)")
        #ans = chat(num_ligne, dico_questions, sheet_candidats_CDG)   ### TO_UNLOCK
        print("Fonction non active, unlock ligne 1018, le tel donné est le {} le nom est {} {}".format(tel, infos_candidat.get(entete_used.get("Prenom")), infos_candidat.get(entete_used.get("Nom"))))
        tospeak("Vous essayez de téléphoner a {} {}".format(infos_candidat.get(entete_used.get("Prenom")), infos_candidat.get(entete_used.get("Nom"))))
        #fill_gs(num_ligne, index_00, ans)   ### TO_UNLOCK
    except ValueError:
        print("Vous n'avez pas saisi un nombre.")
    except AssertionError:
        print("Error: Le numero de ligne saisi est inférieure à 2 ou supérieur à {}.".format(max_num_ligne))

########### FIN PREPARATION A LA CONVERSATION
        
########### Scheduler

def get_event():
    '''
    Recupere le prochain evenement
    Return:
        STRING : contenant la date et l'heure de l'event (start)
        STRING : contenant la date et l'heure de Maintenant (nowi)
        Objet event : contient le futur event trouvé
    OU
        None si aucun event futur (aucun event ou que des events passés trouvés)
        
    '''
    global service_CDG
    #connect_to_GA()
    nowi = datetime.datetime.now().isoformat() + 'Z' # 'Z' indicates UTC time
    nowi=nowi[0:19]+"+02:00" #on change le format de la date pour que cela corresponde � l'event
    print('Getting the upcoming event, now_is', nowi[0:16])
    events_result = service_CDG.events().list(calendarId='primary', timeMin=nowi, #on va chercher le prochain event
                                    maxResults=4, singleEvents=True,
                                    orderBy='startTime').execute()
    events = events_result.get('items', [])#on va chercher le prochain event
    if not events:
        print('No upcoming events found.') #si il n'y a pas d'�venement
        return None, None, None
    else:
        for event in events:      
            start = event['start'].get('dateTime', event['start'].get('date')) #definit le prochain event
            print("start_event :", start[0:16])
            if start[0:16] >= nowi[0:16]:
                break
        if start[0:16] < nowi[0:16]:
            print("No future events")
            return None, None, None
        print(start, event['summary'])#print l'event
        print('l event est �  ..')
        print(start[0:16]) #print la forme simplifi�e de l'event � la minute
        print('Il est ..')
        print(nowi[0:16]) #print la forme simplifi�e de l'heure actuelle � la minute
        print(event['description'])
        return start, nowi, event
        
def real_job():
    '''
    Surveille l'heure de l'event afin de lancer le call
    '''
    start, nowi, event = get_event()
    if start == None:
        print("No event")
    elif start[0:16]==nowi[0:16]: #check si ces deux heures correspondent
        print("enfin l'event :", event['description'])
        '''
        message = client.messages \
            .create(
                 body="Si tu ne transf�res pas ce message � au moins 400 de tes 100 contacts, tu n'auras pas perdu 100 sms pour rien... A toi de jouer !",
                 from_='+33644600945',
                 to=event['description']
             )#envoi un sms au num�ro en description si cela concorde
            '''
        #print(message.sid)
        main_conv(event['description'])

    #schedule.every(1).minutes.do(real_job)

def sched_bot():
    '''
    Maintient le scheduler, permet de relancer les requetes vers GA afin d'actualiser les events
    '''
    s = sched.scheduler(time.time, time.sleep)
    j=""
    while j!="ok":
        s.enter(30, 1, real_job)
        s.run()
        #j = input("stop ? taper ok ")
        #schedule.run_pending()
        #time.sleep(20)
        
########### FIN Scheduler

#%% 
###############################################################################
################################### MAIN ######################################
###############################################################################



# Appel de fonctions

if __name__ == "__main__":
    Connections(scope)
    main_conv("07 78 07 49 03")
    sched_bot()

    '''
    fill_gs(num_ligne, index_00, ans)
    delete_gs_fill_answers(num_ligne, index_00, ans)
    '''
    #d_connect_to_mongo('mongodb://localhost:27017/', "BCGD", "VBot_Vivier")
    #d_answer_to_mongo(ans)
    one_candidat_to_mongo("778074903")
    #GS_to_mongo("VBot_VivierfromGS")
    '''
    
    delete_mongo_collec("VBot_Vivier") # "VBot_VivierfromGS"
    mongo_to_df("VBot_Vivier")
    '''