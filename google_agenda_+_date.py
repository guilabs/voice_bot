import time
import calendar
import timestring #converts string to datetime (english only)
from translate import Translator #translates (so we can use timestring in french)
from datetime import timedelta, datetime
import datetime
import time
import schedule
import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import datetime     # for general datetime object handling
import rfc3339      # for date object -> date string
import iso8601      # for date string -> date object

def get_date_object(date_string):
    return iso8601.parse_date(date_string)
def get_date_string(date_object):
    return rfc3339.rfc3339(date_object)

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/calendar']

" this function take tahe parameter date"
def maino(date):
    """Shows basic usage of the Google Calendar API.
    Prints the start and name of the next 10 events on the user's calendar.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'): # cela permet de checker si le fichier pickle n'a pas été déjà créé
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request()) 
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token: # cela permet de recréer le fichier pickle
            pickle.dump(creds, token)

    service = build('calendar', 'v3', credentials=creds)
    
    date=str(date).replace(" ","T")+"+02:00"
    print(date)
    test_date = get_date_object(date) 
    test_date=test_date+timedelta(minutes=15)# ajoute 15 minutes à l'input de la date
    test_string = get_date_string(test_date)# retransforme en date internet
    print('Getting the upcoming event')
    events_result = service.events().list(calendarId='rfns5k5a3eefhg2ooi7288iqkg@group.calendar.google.com', timeMin=date,
                                        maxResults=1, singleEvents=True,
                                        orderBy='startTime').execute()
    events = events_result.get('items', [])
    # Call the Calendar API

    if not events:
        print('No upcoming events found.')
    for event in events:
        start = event['start'].get('dateTime', event['start'].get('date')) #affiche l'event qui viendra à posterieuri de la date donnée
        print('upcoming event')
        print(start, event['summary'])
        print(start[0:16])

    print("scheduling time")
    print(date[0:16])
    print("event time")
    print(start[0:16])
    title = "Panam All Star"
    if start[0:16]!=date[0:16]: #si l'evenement d'après est le même que celui donné, alors on ne créé pas l'event
        evenoto = {
  'summary': title,
  'location': '800 Howard St., San Francisco, CA 94103',
  'description': 'A chance to hear more about Google\'s developer products.',
  'start': {
    'dateTime': date,
    'timeZone': 'Europe/Paris',
  },
  'end': {
    'dateTime': test_string,
    'timeZone': 'Europe/Paris',
  },
  'recurrence': [
    'RRULE:FREQ=DAILY;COUNT=1'
  ],
  'attendees': [
    {'email': 'guillaumelbr13@gmail.com'},
  ],
  'reminders': {
    'useDefault': False,
    'overrides': [
      {'method': 'email', 'minutes': 24 * 60},
      {'method': 'popup', 'minutes': 10},
    ],
  },
}
        event = service.events().insert(calendarId='rfns5k5a3eefhg2ooi7288iqkg@group.calendar.google.com', body=evenoto).execute()
        print('Event created: %s' % (event.get('htmlLink')))
        var = "empty"
    else:
        print("This slot is already taken")
        var = "pas empty"
    return var
