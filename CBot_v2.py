# -*- coding: utf-8 -*-
"""
Created on Sat May 11 08:03:31 2019

@author: David
"""

# A FAIRE :
'''
IMPORTANT !!!

#### Modifier dico pour insertion GS correcte !!!
#### relier et creer l'algo qui permettrait de prendre rdv

SECONDAIRE

#### Modifier INPUT par ask_listen_main dans get_answers !!!
#### Modifier INPUT par ask_listen_begin dans chat !!!

#### Creation de la fonction : def create_dico_q() et virer les connecs qui sont a la main :
    On ne devrait avoir que 5 parties : iumports, variables, def, main, tests (a virer a la fin)

#### Modifier GS (remettre la question 09, replacer dans l'ordre, remplir civilité, ...)
09-Dans quelle ville êtes-vous ?

#### Commenter fonctions

#### Voir comment les speaks gerent les dates, dans les 2 sens, lecture ecriture

#### PLUS UTILE : Creer le calendrier en sheet 2

#### Gerer les manips dans mongo

'''
###############################################################################
################################### IMPORTS ###################################
###############################################################################

import win32com.client
from pygame import mixer
import speech_recognition as sr  
from os import system
# import os
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import datetime
#from datetime import date
import pymongo
import pandas as pd

'''
# use creds to create a client to interact with the Google Drive API
scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', scope)
client = gspread.authorize(creds)
sheet1 = client.open("Dataset - Voicebot").sheet1
# Micro
speaker = win32com.client.Dispatch("SAPI.SpVoice")
'''
###############################################################################
################################### VARIABLES #################################
###############################################################################

# Il est risqué de modifier le dictionnaire ci-dessous, attention de bien faire matcher les cle chiffrées de GS avec lui

#le client peut ajouter plus de questions dans ce dico s'il ajoute une "cle" chiffrée dans la googlesheet (GS)
q_ref = {("00","rep-gr"):"",
("01","statut_14"):"Quel est votre statut actuel Veuillez choisir entre: CDI , CDD , Freelance , Stagiaire , Alternant",
("02","secteur"):"Dans quel secteur évoluez-vous?",
("03","annee_exp"):"Quel est votre nombre d’année d’expérience ?",
("04","poste"):"Quel poste occupez-vous actuellement ?",
("05","correspond_06"):"Est-ce que votre poste actuel vous correspond ?, repondez par oui ou non",
("06","poste_souhaite_c"):"Sur quel type de poste souhaiteriez-vous évoluer?",
("07","competences"):"Veuillez citer 3 de vos compétences. Par exemple: Python, Gestion de projet, Organisé",
("08","structure"):"Dans quel type de structure préférez-vous travailler ? Veuillez choisir entre: Petite,  Grande, ou Indifférent",
("09","ville"):"Dans quelle ville travaillez vous?",
("10","lieu"):"Quelles sont vos préférences concernant le lieu de travail ? Veuillez choisir entre: Sur site , à distance, ou Indifférent",
("11","mobile"):"Etes-vous mobile ? Veuillez choisir entre: Ville, Pays, ou International",
("12","rech_active_13"):"Etes vous en recherche active ou bien à l’écoute du marché ou pas du tout en recherche ?",
("13","disponibilite"):"Quand êtes-vous disponible ?",
("14","pretentions_c"):"Quelles sont vos prétentions salariales ?",
("15","TJM_c"):"Quel est votre taux journalier moyen ?"
}

d_mois = {"01":"janvier", "02":"fevrier", "03":"mars", "04":"avril", "05":"mai", "06":"juin", "07":"juillet", "08":"aout", "09":"septembre", "10":"octobre", "11":"novembre", "12":"decembre"}
d_jour = {"01":"premier", "02":"deux", "03":"trois", "04":"quatre", "05":"cinq", "06":"six", "07":"sept", "08":"huit", "09":"neuf", "10":"dix", "11":"onze", "12":"douze", "13":"treize", "14":"quatorze", "15":"quinze", "16":"seize", "17":"Dix-sept", "18":"Dix-huit", "19":"Dix-neuf", "20":"vingt", "21":"vingt et un", "22":"vingt deux", "23":"vingt trois", "24":"vingt quatre", "25":"vingt cinq", "26":"vingt six", "27":"vingt sept", "28":"vingt huit", "29":"vingt neuf", "30":"trente", "31":"trente et un"}
d_weekday = {"Monday":"lundi", "Tuesday":"mardi", "Wednesday":"mercredi", "Thursday":"jeudi", "Friday":"vendredi", "Saturday":"samedi", "Sunday":"dimanche"}

# A paramètrer

pathformp3 = "C:\Formation\Simplon-Dev_Data_IA\ML\Projet_Fin\Audio"

'''
dico_ref = {'00':'rep-gr','01':'statut_14','02':'secteur','03':'annee_exp','04':'poste','05':'correspond_06',
            '06':'poste_souhaite_c','07':'competences','08':'structure','09':'ville','10':'lieu',
            '11':'mobile','12':'rech_active_13','13':'disponibilite','14':'pretentions_c','15':'TJM_c'}

l_dico_ref = []
for i in dico_ref.keys():
    l_dico_ref.append(i)
lcles0 = []
for i in from_gs:lcles0.append(i[:2])
for s in lcles0:
    if s not in l_dico_ref[0]:print("Nouvelle valeur dans la googlesheet non référencée, complétez le dico 'dico_ref' ci-dessus")
lcles1 = []
for i in lcles0:
    lcles1.append(q_ref.get(i))
lvals = []
for i in lcles1:
    lvals.append(q_ref.get(i))
dico_questions = {}
if len(lcles0) == len(lcles1) == len(lvals):
    for i in range(len(lcles0)):
        dico_questions.update({(lcles0[i],lcles1[i]):lvals[i]})
else:print('len different between the 3 lists above')
index_00 +=1
'''


#%%
###############################################################################
################################### VARIABLES #################################
###############################################################################

########### Divers
def Connections(sp_rate=0.9, sp_volume=100):
    '''
    Used by main, No sub
    Lance toutes les connections aux divers services
    params:
        FLOAT : vitesse de la parole de l'ordi
        INT : volume de la parole de l'ordi
    return : 
        objet sheet de google sheet
        objet speaker du micro
    '''
    scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', scope)
    client = gspread.authorize(creds)
    sheet1 = client.open("Dataset - Voicebot").sheet1
    speaker = win32com.client.Dispatch("SAPI.SpVoice")
    speaker.Rate = sp_rate
    speaker.Volume = sp_volume
    return sheet1, speaker
        
    
def tospeak(text):
    '''
    Used by main, Sub
    permet a l'ordi de parler
    params : STRING : ce que vous voulez que l'ordi dise
    '''
    speaker.Speak(text)

def play_mp3(path, fic):
    """
    Non utilisé pour le moment
    Play un fichier son mp3
    params :
        STRING : path : chemin absolu ou se trouve le fichier mp3
        STRING : fic :  nom du fichier audio, pas besoin de l'extension
    """
    if "." in fic:
        fic = fic.split(".", 1)[0]
    fic = path + "/" + fic + ".mp3"
    mixer.init()
    mixer.music.load(fic)
    mixer.music.play()
    
########### FIN Divers
    
########### Questionnaire interaction

def create_dico_q():
    '''
    Used by main, No sub
    Creation du dico (final) des questions a poser, A lancer a chaque modification des en-tete de colonne dans la googlesheet
    return :
        DICT : dico contenant les questions que l'on va poser
        INT : numero de colonne, dans la GS, ou se trouve '00' (permet l'insertion dans la GS au bon endroit)
    '''
    from_gs = sheet1.row_values(1)
    index_00 = from_gs.index([i for i in from_gs if i.startswith('00')][0])
    del from_gs[0:index_00]
    lcles0 = []
    for i in from_gs:lcles0.append(i[:2])

    dico_questions = {}
    for i in lcles0:
        temp = [v for v in q_ref.items() if v[0][0]==i]
        dico_questions.update({temp[0][0]:temp[0][1]})    
    
    return dico_questions, index_00

def decoderep(nrj_threshold=100, duree=3):
    '''
    Sub (ask_listen)
    Valide les propos (en rapport avec le questionnaire)
    params : 
        INT : qualité audio
        INT : temps de reponse
    return : la validation
    '''
    rrep = sr.Recognizer()
    with sr.Microphone() as sourcerep:
        rrep.dynamic_energy_threshold = nrj_threshold # False or 50-4000 are good parameters
        rrep.adjust_for_ambient_noise(sourcerep, duration = 1)
        tospeak('Vous confirmez ?, répondez par exactement ou faux, c\'est a vous')
        audio = rrep.record(sourcerep, duration = duree, offset = None)
        #tospeak('analyse de votre confirmation')
        rep = rrep.recognize_google(audio, language='fr-FR', show_all=False)
        tospeak('vous venez de dire '+rep) # A voir si on garde ou vire
        return rep	

def ask_listen(question, nrj_threshold = 100, timeo = 4, duree = 4): # wavefile
    '''
    Sub (ask_listen_main, get_answers)
    Prend en charge 1 question du questionnaire
    params :
        STRING : la question
        INT : divers qualité audio : threshold, timeo
        INT : duree d'enregistrement (temps laissé pour la reponse)
    return :
        STRING : la reponse
        False si reponse incomprise
    '''
    string = ""
    r = sr.Recognizer()
    with sr.Microphone(device_index=None) as source: # WavFile(wavefile)
        r.dynamic_energy_threshold = True
        r.energy_threshold = nrj_threshold # False or 50-4000 are good parameters
        r.adjust_for_ambient_noise(source, duration = 1)
        tospeak(question)
        system("{}".format(question))
        #audio = r.listen(source, timeout = timeo)
        audio = r.record(source, duration = duree, offset = 0.1)
        #tospeak('j\'analyse votre réponse')
        #system("say Analyse de votre réponse")
        try:
            string = ''.join(r.recognize_google(
                audio, language='fr-FR', show_all=False))
            tospeak('votre réponse est '+string)
            system("say votre réponse est {}".format(string))
            rep = decoderep(nrj_threshold, 3)
            if rep in ['exactement', 'exact', 'xactement', 'oui'] or rep.endswith(("men","man","ment","mand","mant")):
                tospeak('votre réponse est donc %s' % string)
                system("say votre réponse est donc {}".format(string))
                return string
            else:
                return False
        except sr.UnknownValueError:
            return False
    return string

def ask_listen_main(question, ds_nrj=100, ds_timeo=4, ds_duree=4):
    '''
    Sub (get_answers)
    Gere les incomprehensions provenant de la parole (en rapport avec le questionnaire)
    params :
        STRING : la question
        INT : divers qualité audio : threshold, timeo
        INT : duree d'enregistrement (temps laissé pour la reponse)
    return :
        True si la reponse est comprise
        False : Sort au bout de 3 echecs
    '''
    compris = False # (WAVFILE)
    j=0
    while not compris:
        compris = ask_listen(question, ds_nrj, ds_timeo, ds_duree)
        if not compris:
            if j<2:
                tospeak("N'ayant pas obtenu une réponse satisfaisante, je repose la question")
        j+=1
        if j == 3:
            tospeak("Je n'ai toujours pas compris, tant pis pour cette réponse, passons a la question suivante")
            break
    tospeak('question suivante')
    return compris

##################

def cond_ask(question_c, answers):
    '''
    Sub(get_answers)
    gere les specificites du questionnaire, les questions ayant des conditions
    param :
        STRING : Numero de la question (ex : 01 ou 14)
        DICT : dico possedant les reponses deja obtenue, l'idee etant d'avoir des conditions sur ces reponses
    return : Boolean
        True si la question concernée doit etre posée sinon False
    '''
    if question_c == "06":
        if answers.get(('05', 'correspond_06')).lower() != "non":
            return False
    elif question_c == "14":
        l1 = ["cdi", "cdd", "stagiaire", "alternant"]
        if answers.get(('01', 'statut_14')).lower() not in l1:
            return False
    elif question_c == "15":
        l2 = ["freelance", "silence", "lance", "lens"]
        if answers.get(('01', 'statut_14')).lower() not in l2:
            return False
    return True

def get_answers(question_dict):
    """
    Sub (chat)
    Gere tout le questionnaire et retourne le dico des reponses obtenues :
        returns a dico with key:name of the question, value:answer to the question
    params :
        DICT : dico contenant les questions a poser
    return :
        DICT : dico avec les reponses            
    """
    answers = {}
    del question_dict[('00', 'rep-gr')]
    for question in question_dict:
        if question[0] in ["06", "14", "15"]:
            ask = cond_ask(question[0], answers)
        else: ask = True
        if ask:
            try:
                answers.update({question:input(question_dict[question])}) #############INPUT########
            except:
                print("Je n'ai pas pu récuperer la réponse à cette question, mais tant pis je passe à la suite")
        else:
            answers.update({question:"Non applicable"})
    return answers
'''
def ask(nom_question,question,dico):
	"""asks question and puts it the answer a dico"""
	#system(question)
	answer = ask_listen_main(question)
	dico.update({nom_question:answer})
'''

########### FIN Questionnaire interaction

########### Chat et debut conversation

def ask_listen_begin(question, nrj_threshold = 100, timeo = 4, duree = 4):
    '''
    Sub (chat)
    Prend en charge 1 question du chat
    params :
        STRING : la question
        INT : divers qualité audio : threshold, timeo
        INT : duree d'enregistrement (temps laissé pour la reponse)
    return :
        STRING : la reponse
        None si reponse incomprise
    '''
    answer = ""
    r = sr.Recognizer()  
    with sr.Microphone(device_index=None) as source:
        r.dynamic_energy_threshold = True
        r.energy_threshold = nrj_threshold
        r.adjust_for_ambient_noise(source, duration = 1)
        tospeak(question)
        audio = r.record(source, duration = duree, offset = 0.1)
        try:
            answer = r.recognize_google(audio, language='fr-FR')
            print("Vous avez dit : {}".format(answer))
        except sr.UnknownValueError:
            return None
    return answer

def chat(num_ligne, question_dict, sheet1):
    '''
    Used by main, No sub
    gere la conversation avec l'interlocuteur, du "bonjour" au "au revoir"
    params :
        INT : numero de ligne, dans la Google Sheet (GS), de la personne appelée
        DICT : dico des questions a poser
        Obj Sheet de GS : la sheet ou se trouve :
            les personnes a appeler
            le questionnaire avec les cles chiffrées
    return :
        DICT : dico avec les réponses
    '''
    #answers = {"name":name}#pour stocker les réponses
    echanges = {"greetings":"je suis Boris, un robot developpé par des étudiants pour récolter de l'information. Est-ce que vous auriez 5 minutes pour répondre à quelques questions concernant votre situation professionnel? Veuillez repondre clairement par oui ou non",
"yes":"Merci, commençons",
"no":"Cela ne prendra que 5 minutes! Voulez-vous continuer ?",
"r1":"Je n'ai pas compris votre réponse, Avez vous 5 minutes a m\'accorder ? répondez par oui ou non",
"r_non":"Je n'ai pas compris votre réponse,Cela ne prendra que 5 minutes! Voulez-vous continuer ?",
"fin_non":"J\'ai pris note de votre refus, passez une bonne journée, au revoir",
"fin_pc":"Je suis désolé, je n'ai toujours pas compris. Je vais raccrocher, je tenterai de vous rappeler a un autre moment. Merci",
"fin_oui":"Merci beaucoup de nous avoir accordé votre temps! Bonne journée et bonne continuation"
}
    answers = {}
    name = sheet1.cell(num_ligne, 2).value + ' ' + sheet1.cell(num_ligne, 4).value
    tospeak("Bonjour {}".format(name))
    oui = ['oui','oui oui', 'ok', 'pas de souci', 'okay','d\'accord','ouais','wesh','bon d\'accord','alrighty then', 'yes', 'pourquoi pas', 'pas de problème']
    non = ['non', 'non non','pas disponible','je suis occupé','etc', 'pas du tout','get lost', 'désolée','non désolée',"j'ai pas le temps", "pas vraiment","pas trop", "pas beacoup","absolument pas",'nope']
	#### demander a la personne si elle veut continuer ####
    rep_gr = input(echanges["greetings"])   ### ask_listen_begin
    indice_pc = 0
    indice_non = 0
    
    while indice_pc < 3 and indice_non < 2:
           
        if rep_gr.lower() in oui:
            tospeak(echanges["yes"])
            system(echanges["yes"])
            #answers.update({('00', 'rep-gr'):"oui"})
            ####### QUESTIONNAIRE ###########
            answers.update(get_answers(question_dict.copy()))
            #################################
            tospeak(echanges["fin_oui"] + " {}".format(name))
            system(echanges["fin_oui"])   ######
            break
        
        elif rep_gr.lower() in non:
            indice_non += 1
            #system("say Cela ne prendra seulement 5 minutes! Voulez-vous continuer?")
            rep_gr = input(echanges["no"])   ### ask_listen_begin
            if rep_gr in non:
                # ask_time() # Prise de rdv ?
                break
        
        else:
            while rep_gr not in oui and rep_gr not in non or rep_gr == None:
                indice_pc += 1
                if indice_pc > 2:
                    break
                #system("say pas compris, Je vais répéter la question")
                if indice_non == 0:
                    rep_gr = input(echanges["r1"])# ask again   ### ask_listen_begin
                else:
                    rep_gr = input(echanges["r_non"])   ### ask_listen_begin
                    if rep_gr in non:
                        indice_non += 1
                        # rep = ask_listen_begin("seriez vous d'accord pour que je vous rappelle a un moment qui vous convient ?")
                        # if rep in oui:
                              # ask_time() # Prise de rdv ?
                        break
    # End while

    if rep_gr.lower() in non:
        tospeak(echanges["fin_non"])
        system("say"+echanges["fin_non"]) ######
        answers.update({('00', 'rep-gr'):"non"}) 
        return answers ### EN CAS DE REFUS : RETOUR NON ###
          
    elif rep_gr.lower() not in oui and rep_gr.lower() not in non:
        tospeak(echanges["fin_pc"])
        system("say"+echanges["fin_pc"]) ######
        answers.update({('00', 'rep-gr'):False})
        return answers ### EN CAS D'INCOMPREHENSION : RETOUR FALSE ###

    return answers

########### FIN Chat et debut conversation
    
########### Google Sheet

def fill_gs(num_row, index_first_ans, dic_answers): # pas de param sheet !!! etrange !!! a voir car si pas necessaire, peut etre peut etre viré des autres def
    '''
    Used by main, No sub
    Rempli la GS (en fonction des reponses obtenue) sur la ligne de la personne appelée
    params :
        INT : numero de la ligne, dans la GS, de la personne concernée
        INT : numero de colonne, dans la GS, ou on commence a ecrire (en theorie cela devrait etre la colonne avec '00')
        DICT : dictionnaire des reponses obtenues
    '''
    index_first_ans += 1
    if (('00', 'rep-gr') in dic_answers):
        sheet1.update_cell(num_row, index_first_ans, dic_answers.get(('00', 'rep-gr')))
    else:
        sheet1.update_cell(num_row, index_first_ans, "oui")
        for i in range(len(dic_answers)):
            sheet1.update_cell(num_row, i+index_first_ans+1, list(dic_answers.values())[i])
    sheet1.update_cell(num_row, 1, str(datetime.datetime.now()))
    
def delete_gs_fill_answers(num_row, index_first_ans, dic_answers): # pas de param sheet !!! etrange !!! a voir car si pas necessaire, peut etre peut etre viré des autres def
    '''
    Used by main, No sub
    Delete les reponses (la ligne), dans la GS, de la personne concernée
    !!! ATTENTION !!! si une modification des colonnes a été faite dans la GS, cette fonction risque d'effacer des données autres que les reponses
    params :
        INT : numero de la ligne, dans la GS, de la personne concernée
        INT : numero de colonne, dans la GS, ou on commence a effacer (en theorie cela devrait etre la colonne avec '00')
        DICT : dictionnaire des reponses obtenues
    '''
    index_first_ans += 1
    for i in range(len(dic_answers)+1):
        sheet1.update_cell(num_row, i+index_first_ans, "")
    sheet1.update_cell(num_row, 1, "")

########### FIN Google Sheet
    
########### Date to speak
    
def date_form_to_speak(la_date, separator_date="-"): # pas de param sur les 3 dicos, il se peut qu'ils soient necessaire
    '''
    Sub (speak_date)
    A partir d'une date sous forme string, prepare la phrase a dire concernant cette date
    !!! ATTENTION !!! No much control on that string, please follow rules in params
    param :
        STRING : la date sous forme yyyy-MM-dd ou yyyy/MM/dd
        STRING : le symbole separant les jours, mois et annee, ex : "-" ou "/"
    return :
        STRING : la phrase prete a etre parlée
    '''
    sep = separator_date
    if sep == "-":
        sep_speak = "les tirets"
    elif sep == "/":
        sep_speak = "la barre oblique"
    else:
        sep_speak = "le symbole de separation"

    if sep in la_date:
        year, month, day = (int(x) for x in la_date.split(sep))   
        rep = datetime.date(year, month, day)
        le_weekday = rep.strftime("%A")
        le_weekday = d_weekday.get(le_weekday)
        le_jour = d_jour.get(la_date.split(sep)[2])
        le_mois = d_mois.get(la_date.split(sep)[1])
        le_annee = la_date.split(sep)[0]
        if len(le_annee) != 4:
            if len(le_annee) == 2:
                le_annee = "20"+le_annee
            else:
                return "Erreur, format de l'année incorrecte"
        
        date_to_speak = le_weekday + " " + le_jour + " " + le_mois + " " + le_annee
    else:
        return "Erreur, format de la date incorrecte, manque {}".format(sep_speak)
    return date_to_speak

def speak_date(param_dt):
    '''
    Used by main, No sub
    Parle la date et l'heure qui doivent etre sous un format précis
    params :
        STRING, format : "yyyy-MM-dd?HH:mm*", sachant que ? correspond a 1 caractere et * a plusieurs (peu importe lesquels)
    '''
    la_date = param_dt[:10]
    l_heure = param_dt[11:16]
    tospeak(date_form_to_speak(la_date) + " a " + l_heure)
    
########### FIN Date to speak
    
########### MongoDB
def d_answer_to_mongo(path, bdd_name, bdd_collec, dic):
    '''
    Used by main, No sub
    Insere un dico dans une base MongoDB
    params :
        STRING : le chemin de la base, generalement : 'mongodb://localhost:27017/'
        STRING : le nom de la base, ici : "BCGD"
        STRING : le nom de la collection, ici : "VBot_Vivier"
        DICT : le dictionnaire a inserer
    '''
    # Creation de la database, collection, insertion
    # A noter qu'une db mongo ne sera pas créée tant qu'elle ne contient pas un 'document'
    conn_mdb_str = path
    myclient = pymongo.MongoClient(conn_mdb_str) # creation d'une instance mongo
    mydb = myclient[bdd_name] # creation de la db
    mycollec = mydb[bdd_collec] # creation de la collection
    # Pour insérer un dico
    mycollec.insert_one(dic)
    
def delete_mongo_collec(path, bdd_name, bdd_collec):
    '''
    Used by main, No sub
    Vide la collection dans une base MongoDB
    params :
        STRING : le chemin de la base, generalement : 'mongodb://localhost:27017/'
        STRING : le nom de la base, ici : "BCGD"
        STRING : le nom de la collection, ici : "VBot_Vivier"
    '''
    conn_mdb_str = path
    myclient = pymongo.MongoClient(conn_mdb_str)
    mydb = myclient[bdd_name]
    mycollec = mydb[bdd_collec]
    # Suppression d'une collection (des data de la table)
    mydb.drop_collection(bdd_collec)
    # Suppression de la db
    #myclient.drop_database('BCGD')
    
def mongo_to_df(path, bdd_name, bdd_collec):
    '''
    Used by main, No sub
    Recupere la collection d'une base MongoDB dans un DF
    params :
        STRING : le chemin de la base, generalement : 'mongodb://localhost:27017/'
        STRING : le nom de la base, ici : "BCGD"
        STRING : le nom de la collection, ici : "VBot_Vivier"
    return :
        DF : dataframe contenant les infos qui se trouvaient dans la collection de la base Mongo
    '''
    conn_mdb_str = path
    myclient = pymongo.MongoClient(conn_mdb_str)
    mydb = myclient[bdd_name]
    mycollec = mydb[bdd_collec]
    # Pour voir les documents dans un dataframe
    #data = mydb.data
    df = pd.DataFrame(list(mycollec.find()))
    return df
########### FIN MongoDB

#%% 
###############################################################################
################################### MAIN ######################################
###############################################################################

# Appel de fonctions

if __name__ == "__main__":
    # On lance les connections necessaires
    sheet1, speaker = Connections(sp_rate=0.9, sp_volume=100)
    # On recupere le nb de lignes max de personnes ecrites dans la GS
    max_num_ligne = len(sheet1.col_values(2))
    # On crée le dico des questions que l'on va poser et qui est basé sur les colonnes de la GS
    dico_questions, index_00 = create_dico_q()
    # On initialise le dico qui va contenir nos reponses
    ans = {}
    # Saisir le numero de ligne, dans la GS, de la personne a appeler
    num_ligne = input("Saisissez un numero de ligne supérieur a 1 :\n")
    try:
        num_ligne = int(num_ligne)
        assert num_ligne > 1
        assert num_ligne < max_num_ligne+1
        ans = chat(num_ligne, dico_questions, sheet1)
    except ValueError:
        print("Vous n'avez pas saisi un nombre.")
    except AssertionError:
        print("Error: Le numero de ligne saisi est inférieure à 2 ou supérieur à {}.".format(max_num_ligne))
    
    '''   
    fill_gs(num_ligne, index_00, ans)
    delete_gs_fill_answers(num_ligne, index_00, ans)
    '''
    d_answer_to_mongo('mongodb://localhost:27017/', "BCGD", "VBot_Vivier", ans)
    '''
    delete_mongo_collec('mongodb://localhost:27017/', "BCGD", "VBot_Vivier")
    mongo_to_df('mongodb://localhost:27017/', "BCGD", "VBot_Vivier")
    '''

###############################################################################
################################### TESTS #####################################
###############################################################################

    # Prise de rdv via agenda
    speak_date(GA3)
    GA1 = "2020-01-17T13:15:00.00000Z"
    GA2 = "2019-11-05T17:15:00+02:00"
    GA3 = "2019-05-20 19:00:00"