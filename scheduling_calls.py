from __future__ import print_function
import time
import schedule
import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
# Download the helper library from https://www.twilio.com/docs/python/install
from twilio.rest import Client
# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/calendar']

# Your Account Sid and Auth Token from twilio.com/console
# DANGER! This is insecure. See http://twil.io/secure
account_sid = '???'
auth_token = '???'
client = Client(account_sid, auth_token)
def main():
    """Shows basic usage of the Google Calendar API.
    Prints the start and name of the next 10 events on the user's calendar.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('calendar', 'v3', credentials=creds)

    # Call the Calendar API
    print('starting')
    def job():
        nowi = datetime.datetime.now().isoformat() + 'Z' # 'Z' indicates UTC time
        nowi=nowi[0:19]+"+02:00" #on change le format de la date pour que cela corresponde à l'event
        print('Getting the upcoming event')
        events_result = service.events().list(calendarId='primary', timeMin=nowi,#on va chercher le prochain event
                                        maxResults=1, singleEvents=True,
                                        orderBy='startTime').execute()
        events = events_result.get('items', [])#on va chercher le prochain event
        if not events:
            print('No upcoming events found.') #si il n'y a pas d'évenement
        else:
            for event in events:
                start = event['start'].get('dateTime', event['start'].get('date')) #definit le prochain event
            #print(start, event['summary'])#print l'event
            print('l event est à  ..')
            print(start[0:16]) #print la forme simplifiée de l'event à la minute
            print('Il est ..')
            print(nowi[0:16]) #print la forme simplifiée de l'heure actuelle à la minute
            print(event['description'])
            if start[0:16]==nowi[0:16]: #check si ces deux heures correspondent
                print(event['description'])
                CBot.main_conv(CBot.format_tel(event['description']))
                """message = client.messages \
                    .create(
                         body="Si tu ne transfères pas ce message à au moins 400 de tes 100 contacts, tu n'auras pas perdu 100 sms pour rien... A toi de jouer !",
                         from_='+00000000000',
                         to=event['description']
                     )"""#envoi un sms au numéro en description si cela concorde"""
                #print(message.sid)
    schedule.every(1).minutes.do(job)
    while True:
        schedule.run_pending()
        time.sleep(1)

if __name__ == '__main__':
    main()
    
    








