from __future__ import print_function
import requests
import threading,queue
import time
from flask import Flask, request
import time
from twilio.twiml.voice_response import VoiceResponse, Gather, Say
from twilio.twiml.messaging_response import MessagingResponse, Message
import time
from os import system
import os 
import dialogflow
import schedule
import datetime
import pickle
import os.path
from twilio.rest import Client
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
# Download the helper library from https://www.twilio.com/docs/python/install
from twilio.rest import Client
bot=""
human=""
q = queue.Queue() # this is the queue for human 
qu = queue.Queue()# this is the queue for bot 
# If modifying these scopes, delete the file token.pickle.
credential_path = "C:/Users/guill/Documents/Projects/JavaScript/twilio-pa/dialog/credentials1.json"
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credential_path
SCOPES = ['https://www.googleapis.com/auth/calendar']
creds = None
# The file token.pickle stores the user's access and refresh tokens, and is
# created automatically when the authorization flow completes for the first
# time.
if os.path.exists('token.pickle'):
    with open('token.pickle', 'rb') as token:
        creds = pickle.load(token)
# If there are no (valid) credentials available, let the user log in.
if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
        creds.refresh(Request())
    else:
        flow = InstalledAppFlow.from_client_secrets_file(
            'credentials.json', SCOPES)
        creds = flow.run_local_server()
    # Save the credentials for the next run
    with open('token.pickle', 'wb') as token:
        pickle.dump(creds, token)

service = build('calendar', 'v3', credentials=creds)

app = Flask(__name__)

answer="Oui bonjour, vous m'entendez?"

def detect_intent_texts(project_id, session_id, texts, language_code):
            """Returns the result of detect intent with texts as inputs.

            Using the same `session_id` between requests allows continuation
            of the conversation."""

            import dialogflow_v2 as dialogflow
            session_client = dialogflow.SessionsClient()

            session = session_client.session_path(project_id, session_id)


            for text in texts:
                text_input = dialogflow.types.TextInput(
                    text=text, language_code=language_code)

                query_input = dialogflow.types.QueryInput(text=text_input)
                response = session_client.detect_intent(
                    session=session, query_input=query_input)


                return response.query_result.fulfillment_text


@app.before_first_request
def activate_job():
    def run_job():
        def job():
            nowi = datetime.datetime.now().isoformat() + 'Z' # 'Z' indicates UTC time
            nowi=nowi[0:19]+"+02:00" #on change le format de la date pour que cela corresponde à l'event
            print('Getting the upcoming event')
            events_result = service.events().list(calendarId='primary', timeMin=nowi,#on va chercher le prochain event
                                            maxResults=1, singleEvents=True,
                                            orderBy='startTime').execute()
            events = events_result.get('items', [])#on va chercher le prochain event
            if not events:
                print('No upcoming events found.') #si il n'y a pas d'évenement
            else:
                for event in events:
                    start = event['start'].get('dateTime', event['start'].get('date')) #definit le prochain event
                #print(start, event['summary'])#print l'event
                print('l event est à  ..')
                print(start[0:16]) #print la forme simplifiée de l'event à la minute
                print('Il est ..')
                print(nowi[0:16]) #print la forme simplifiée de l'heure actuelle à la minute
                print(event['description'])
                if start[0:16]==nowi[0:16]: #check si ces deux heures correspondent
                    print(event['description'])
                    # Your Account Sid and Auth Token from twilio.com/console
                    # DANGER! This is insecure. See http://twil.io/secure
                    account_sid = 'ACfa505be65c1d21ae731b6ee9f8eefa60'
                    auth_token = 'a313f00b27cbcafd00370c804eb46b22'
                    client = Client(account_sid, auth_token)
                    call = client.calls.create(
                                            url='http://3fe3e2a4.ngrok.io/answer',
                                            to=event['description'],
                                            from_='+33644644868'
                                        )

                    print(call.sid)
                    #print(message.sid)
        #schedule.every(1).minutes.do(job)
        while True:
            job()
            print("Run recurring task")
            time.sleep(60)

    thread = threading.Thread(target=run_job)
    thread.start()
        
#----------------------------------------------------------------------------------------------------------------        

    def voicebot(bot,q,human,qu):
        
        #le client peut ajouter plus de questions dans ce dico
        que = {"secteur":"say Dans quel secteur évoluez-vous?",
        "exp":"say Quel est votre nombre d’année d’expérience ?",
        "competences":"say Veuillez citer 3 de vos compétences. Par exemple: Java, Python, Gestion de projet",
        "structure":"say Dans quel type de structure préférez-vous travailler ? Veuillez choisir entre: Petite,  Grande, ou Indifférente",
        "ville":"say Dans quelle ville êtes-vous?",
        "lieu":"say Quelles sont vos préférences de lieu de travail ? Veuillez choisir entre: Sur site , à distance, ou Indifférent",
        "mobile":"say Etes-vous mobile ? Veuillez choisir entre: Ville, Pays, ou Internationale",
        }

        def ask_listen(message):
            #can be used to replace input() but gets answers from microphone instead of from the keyboard
            # eg. answer = ask_listen('say your answer')
            answer = ""
            human = qu.get()
            print(message)
            answer=human

        def get_answers(question_dict):
            """returns a dico with key:name of the question, value:answer to the question"""
            global bot
            answers = {}
            for question in question_dict:
                #print(question, "corresponds to", question_dict[question])
                try:
                    bot=(question_dict[question])
                    answers.update({question:ask_listen(question)})
                except:
                    print("Je n'ai pas pu récuperer la réponse à cette question, mais tant pis je passe à la suite")
            return answers

        def ask(nom_question,question,dico):
            global bot
            """asks question and puts it the answer a dico"""
            bot=(question)
            q.put(bot)
            answer = ask_listen(question)
            dico.update({nom_question:answer})


        def chat(name,question_list):
            global bot
            """main chat bot function, returns dico with questions and answers
            if answer not heard > empty string"""
            bot=("say Bonjour, je suis un robot qui travaille pour un cabinet de statistique dans le domaine de l’emploi. Est-ce que vous auriez 5 minutes pour répondre à quelques questions concernant votre situation professionnel? Veuillez repondre clairement par oui ou non")		
            q.put(bot)
            answers = {"name":name}#pour stocker les réponses 
            greeting = "Bonjour {}, je suis Boris, un bot developpé par des étudiants pour récolter de l'information. Est-ce que vous auriez 5 minutes pour répondre à quelques questions concernant votre situation professionnel?\nVeuillez repondre clairement par oui ou non\n".format(name)
            oui = ['oui','oui oui', 'ok', 'pas de souci', 'okay',"d'accord","ouais","wesh","bon d'accord",'alrighty then', 'yes']
            non = ['non', 'non non','pas disponible','je suis occupé','etc', 'pas du tout','get lost', 'désolée','non désolée',"j'ai pas le temps", "pas vraiment","pas trop", "pas beacoup","absolument pas",'nope']
            #### demander la personne si elle veut continuer ####
            rep_gr = ask_listen(greeting)
            print(rep_gr)
            #if detect_repeat(rep_gr, greeting):
                #rep_gr = input(greeting)
            if rep_gr not in oui and rep_gr not in non or rep_gr == None: 
                bot=("say Je n'ai pas compris, Je vais répéter la question")
                q.put(bot)
                rep_gr = ask_listen(greeting)# ask again
            if rep_gr in non:
                bot=("say Cela ne prendra seulement 5 minutes! Voulez-vous continuer?")
                q.put(bot)
                rep_gr = ask_listen("Cela ne prendra seulement 5 minutes! Voulez-vous continuer?")
                if rep_gr not in oui and rep_gr not in non:
                    bot=("say Je suis désolé, je n'ai toujours pas compris. Je vais continuer avec les questions, mais si vous ne souhaitez pas répondre, veuillez racrocher. Merci.")
                if rep_gr in non:
                    print("ok bye")
                    bot=("say D'accord, passez une bonne journée, au revoir!")
                    return None
                    #hang up?
            if rep_gr in oui:
                bot=("say Merci, commençons")
            ####Debut des questions
            ask("statut","say Quel est votre statut actuel Veuillez choisir entre: CDI , CDD , Freelance , Stagiaire , Alternant",answers)
            ##########------##########
            ask("correspond","say Est-ce que votre poste actuel vous correspond?",answers)
            #########------##########
            ask("salaire",'say Quelles sont vos prétentions salariales?',answers)
            ##bye bye	
            bot=('say Merci beaucoup de nous avoir accordé votre temps! Bonne journée et bonne continuation!')
            print(answers)
            print("ok now cheat"+bot)
            return answers,bot

        name = "Guillaume"
        chat(name, que)
        qu.put(bot)
        return bot
    thread = threading.Thread(target=voicebot,args=(bot,q,human,qu))
    thread.start()
    
#----------------------------------------------------------------------------------------------------------------        

@app.route("/")
def hello():
    return "Hello World!"


@app.route('/answer', methods=['GET', 'POST'])
def answer_call():
    resp = VoiceResponse()
    gather = Gather(input="speech", language="fr-FR", timeout="3", action="/gather") #bot talk + wait for human input and then send it to gather url
    gather.say(bot,language='fr-FR')
    resp.append(gather)
    resp.redirect('/answer')
    return str(resp)

@app.route('/gather', methods=['GET', 'POST'])
def gather():
    resp = VoiceResponse() 
    human = request.form['SpeechResult']
    print(human)
    qu.put(human) #we put the human talk into the voicebot thread
    global bot
    bot = q.get() #we retrieve the bot talk from the voicebot thread
    print(bot)
    resp.redirect('/answer')
    return str(resp)

def start_runner(bot,q,human,qu):
    def start_loop(bot,q,human,qu):
        not_started = True
        while not_started:
            print('In start loop')
            try:
                r = requests.get('http://localhost:5000')#http://0.0.0.0:5000 for heroku? #or http://localhost:5000/#http://127.0.0.1:5000/
                if r.status_code == 200:
                    print('Server started, quiting start_loop')
                    not_started = False
                print(r.status_code)
            except:
                print('Server not yet started')
            time.sleep(2)

    print('Started runner')
    thread = threading.Thread(target=start_loop,args=(bot,q,human,qu))
    thread.start()

if __name__ == "__main__":
    start_runner(bot,q,human,qu)
    app.run()