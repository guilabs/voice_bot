from __future__ import print_function
import requests
import threading
import time
from flask import Flask, request
import time
from twilio.twiml.voice_response import VoiceResponse, Gather, Say
from twilio.twiml.messaging_response import MessagingResponse, Message
import time
import os 
import dialogflow
import schedule
import datetime
import pickle
import os.path
from twilio.rest import Client
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
# Download the helper library from https://www.twilio.com/docs/python/install
from twilio.rest import Client
# If modifying these scopes, delete the file token.pickle.
credential_path = "your path"
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credential_path
SCOPES = ['https://www.googleapis.com/auth/calendar']
creds = None
# The file token.pickle stores the user's access and refresh tokens, and is
# created automatically when the authorization flow completes for the first
# time.
if os.path.exists('token.pickle'):
    with open('token.pickle', 'rb') as token:
        creds = pickle.load(token)
# If there are no (valid) credentials available, let the user log in.
if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
        creds.refresh(Request())
    else:
        flow = InstalledAppFlow.from_client_secrets_file(
            'credentials.json', SCOPES)
        creds = flow.run_local_server()
    # Save the credentials for the next run
    with open('token.pickle', 'wb') as token:
        pickle.dump(creds, token)

service = build('calendar', 'v3', credentials=creds)

app = Flask(__name__)

answer="Oui bonjour, vous m'entendez?"

def detect_intent_texts(project_id, session_id, texts, language_code):
            """Returns the result of detect intent with texts as inputs.

            Using the same `session_id` between requests allows continuation
            of the conversation."""

            import dialogflow_v2 as dialogflow
            session_client = dialogflow.SessionsClient()

            session = session_client.session_path(project_id, session_id)


            for text in texts:
                text_input = dialogflow.types.TextInput(
                    text=text, language_code=language_code)

                query_input = dialogflow.types.QueryInput(text=text_input)
                response = session_client.detect_intent(
                    session=session, query_input=query_input)


                return response.query_result.fulfillment_text


@app.before_first_request
def activate_job():
    def run_job():
        def job():
            nowi = datetime.datetime.now().isoformat() + 'Z' # 'Z' indicates UTC time
            nowi=nowi[0:19]+"+02:00" #on change le format de la date pour que cela corresponde à l'event
            print('Getting the upcoming event')
            events_result = service.events().list(calendarId='primary', timeMin=nowi,#on va chercher le prochain event
                                            maxResults=1, singleEvents=True,
                                            orderBy='startTime').execute()
            events = events_result.get('items', [])#on va chercher le prochain event
            if not events:
                print('No upcoming events found.') #si il n'y a pas d'évenement
            else:
                for event in events:
                    start = event['start'].get('dateTime', event['start'].get('date')) #definit le prochain event
                #print(start, event['summary'])#print l'event
                print('l event est à  ..')
                print(start[0:16]) #print la forme simplifiée de l'event à la minute
                print('Il est ..')
                print(nowi[0:16]) #print la forme simplifiée de l'heure actuelle à la minute
                print(event['description'])
                if start[0:16]==nowi[0:16]: #check si ces deux heures correspondent
                    print(event['description'])
                    # Your Account Sid and Auth Token from twilio.com/console
                    # DANGER! This is insecure. See http://twil.io/secure
                    account_sid = 'token'
                    auth_token = 'token'
                    client = Client(account_sid, auth_token)
                    call = client.calls.create(
                                            url='https://6bbcb44c.ngrok.io/answer',
                                            to=event['description'],
                                            from_='+your number'
                                        )

                    print(call.sid)
                    #print(message.sid)
        #schedule.every(1).minutes.do(job)
        while True:
            job()
            print("Run recurring task")
            time.sleep(60)

    thread = threading.Thread(target=run_job)
    thread.start()

@app.route("/")
def hello():
    return "Hello World!"

@app.route('/answer', methods=['GET', 'POST'])
def answer_call():
    resp = VoiceResponse()
    gather = Gather(input="speech", language="fr-FR", timeout="3", action="/gather")
    gather.say(answer,language='fr-FR')
    resp.append(gather)
    resp.redirect('/answer')
    return str(resp)

@app.route('/gather', methods=['GET', 'POST'])
def gather():
    resp = VoiceResponse() 
    texts = request.form['SpeechResult']
    print(texts)
    global answer
    answer = detect_intent_texts("essec-241318","session_test" ,[texts], "French — fr")
    print(answer)
    resp.redirect('/answer')
    return str(resp)

def start_runner():
    def start_loop():
        not_started = True
        while not_started:
            print('In start loop')
            try:
                r = requests.get('http://localhost:5000/')
                if r.status_code == 200:
                    print('Server started, quiting start_loop')
                    not_started = False
                print(r.status_code)
            except:
                print('Server not yet started')
            time.sleep(2)

    print('Started runner')
    thread = threading.Thread(target=start_loop)
    thread.start()

if __name__ == "__main__":
    start_runner()
    app.run()