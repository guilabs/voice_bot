# -*- coding: utf-8 -*-
"""
Created on Sat May 11 08:03:31 2019

@author: Administrateur
"""

# A FAIRE :
'''
#### Modifier INPUT par ask_listen_main dans get_answers !!!
#### Modifier dico pour insertion GS correcte !!!
#### Modifier GS (remettre la question 09, replacer dans l'ordre, remplir civilité, ...)
09-Dans quelle ville êtes-vous ?
#### Commenter fonctions
'''

################################### Imports
# pandas as pd
import speech_recognition as sr  
from os import system
# import os
import win32com.client
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import datetime

# use creds to create a client to interact with the Google Drive API
scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', scope)
client = gspread.authorize(creds)
sheet = client.open("Dataset - Voicebot").sheet1
################################### Variables
# A ne pas toucher

dico_ref = {'00':'rep-gr','01':'statut_14','02':'secteur','03':'annee_exp','04':'poste','05':'correspond_06',
            '06':'poste_souhaite_c','07':'competences','08':'structure','09':'ville','10':'lieu',
            '11':'mobile','12':'rech_active_13','13':'disponibilite','14':'pretentions_c','15':'TJM_c'}
#le client peut ajouter plus de questions dans ce dico s'il ajoute une "cle" chiffrée dans la googlesheet
q_ref = {"rep-gr":"",
"statut_14":"Quel est votre statut actuel Veuillez choisir entre: CDI , CDD , Freelance , Stagiaire , Alternant",
"secteur":"Dans quel secteur évoluez-vous?",
"annee_exp":"Quel est votre nombre d’année d’expérience ?",
"poste":"Quel poste occupez-vous actuellement ?",
"correspond_06":"Est-ce que votre poste actuel vous correspond ?, repondez par oui ou non",
"poste_souhaite_c":"Sur quel type de poste souhaiteriez-vous évoluer?",
"competences":"Veuillez citer 3 de vos compétences. Par exemple: Python, Gestion de projet, Organisé",
"structure":"Dans quel type de structure préférez-vous travailler ? Veuillez choisir entre: Petite,  Grande, ou Indifférent",
"ville":"Dans quelle ville travaillez vous?",
"lieu":"Quelles sont vos préférences concernant le lieu de travail ? Veuillez choisir entre: Sur site , à distance, ou Indifférent",
"mobile":"Etes-vous mobile ? Veuillez choisir entre: Ville, Pays, ou International",
"rech_active_13":"Etes vous en recherche active ou bien à l’écoute du marché ou pas du tout en recherche ?",
"disponibilite":"Quand êtes-vous disponible ?",
"pretentions_c":"Quelles sont vos prétentions salariales ?",
"TJM_c":"Quel est votre taux journalier moyen ?"
}


##################
speaker = win32com.client.Dispatch("SAPI.SpVoice")
speaker.Rate = 0.9
speaker.Volume = 100


# Creation du dico, A lancer a chaque modification des en-tete de colonne dans la googlesheet
l_dico_ref = []
for i in dico_ref.keys():
    l_dico_ref.append(i)
from_gs = sheet.row_values(1)
index_00 = from_gs.index([i for i in from_gs if i.startswith('00')][0])
del from_gs[0:index_00]
lcles0 = []
for i in from_gs:lcles0.append(i[:2])
for s in lcles0:
    if s not in l_dico_ref:print("Nouvelle valeur dans la googlesheet non référencée, complétez le dico 'dico_ref' ci-dessus")
lcles1 = []
for i in lcles0:
    lcles1.append(dico_ref.get(i))
lvals = []
for i in lcles1:
    lvals.append(q_ref.get(i))
dico_questions = {}
if len(lcles0) == len(lcles1) == len(lvals):
    for i in range(len(lcles0)):
        dico_questions.update({(lcles0[i],lcles1[i]):lvals[i]})
else:print('len different between the 3 lists above')
index_00 +=1

dico_questions.keys()

del l_dico_ref, s, from_gs
del lcles0, lcles1, lvals

################################### Fonctions

def tospeak(text):
    speaker.Speak(text)
    
def decoderep(nrj_threshold, duree):
    rrep = sr.Recognizer()
    with sr.Microphone() as sourcerep:
        rrep.dynamic_energy_threshold = nrj_threshold # False or 50-4000 are good parameters
        rrep.adjust_for_ambient_noise(sourcerep, duration = 1)
        tospeak('Vous confirmez ?, répondez par exactement ou faux, c\'est a vous')
        audio = rrep.record(sourcerep, duration = duree, offset = None)
        #tospeak('analyse de votre confirmation')
        rep = rrep.recognize_google(audio, language='fr-FR', show_all=False)
        tospeak('vous venez de dire '+rep)
        return rep	

def ask_listen(question, nrj_threshold = 100, timeo = 4, duree = 4): # wavefile
    string = ""
    r = sr.Recognizer()
    with sr.Microphone(device_index=None) as source: # WavFile(wavefile)
        r.dynamic_energy_threshold = True
        r.energy_threshold = nrj_threshold # False or 50-4000 are good parameters
        r.adjust_for_ambient_noise(source, duration = 1)
        tospeak(question)
        system("{}".format(question))
        #audio = r.listen(source, timeout = timeo)
        audio = r.record(source, duration = duree, offset = 0.1)
        #tospeak('j\'analyse votre réponse')
        #system("say Analyse de votre réponse")
        try:
            string = ''.join(r.recognize_google(
                audio, language='fr-FR', show_all=False))
            tospeak('votre réponse est '+string)
            system("say votre réponse est {}".format(string))
            rep = decoderep(nrj_threshold, 3)
            if rep in ['exactement', 'exact', 'xactement', 'oui'] or rep.endswith(("men","man","ment","mand","mant")):
                tospeak('votre réponse est donc %s' % string)
                system("say votre réponse est donc {}".format(string))
                return string
            else:
                return False
        except sr.UnknownValueError:
            return False
    return string

def ask_listen_main(question, ds_nrj=100, ds_timeo=4, ds_duree=4):
    compris = False # (WAVFILE)
    j=0
    while not compris:
        compris = ask_listen(question, ds_nrj, ds_timeo, ds_duree)
        if not compris:
            if j<2:
                tospeak("N'ayant pas obtenu une réponse satisfaisante, je repose la question")
        j+=1
        if j == 3:
            tospeak("Je n'ai toujours pas compris, tant pis pour cette réponse, passons a la question suivante")
            break
    tospeak('question suivante')
    return compris

def ask_listen_begin(question, nrj_threshold = 100, timeo = 4, duree = 4):
    answer = ""
    r = sr.Recognizer()  
    with sr.Microphone(device_index=None) as source:
        r.dynamic_energy_threshold = True
        r.energy_threshold = nrj_threshold
        r.adjust_for_ambient_noise(source, duration = 1)
        tospeak(question)
        audio = r.record(source, duration = duree, offset = 0.1)
        try:
            answer = r.recognize_google(audio, language='fr-FR')
            print("Vous avez dit : {}".format(answer))
        except sr.UnknownValueError:
            return False
    return answer

##################

def cond_ask(question_c, answers):
    '''
    param : Number of the question, dico answers
    return : boolean
    '''
    if question_c == "06":
        if answers.get(('05', 'correspond_06')).lower() != "non":
            return False
    elif question_c == "14":
        l1 = ["cdi", "cdd", "stagiaire", "alternant"]
        if answers.get(('01', 'statut_14')).lower() not in l1:
            return False
    elif question_c == "15":
        l2 = ["freelance", "silence", "lance", "lens"]
        if answers.get(('01', 'statut_14')).lower() not in l2:
            return False
    return True

def get_answers(question_dict):
    """returns a dico with key:name of the question, value:answer to the question"""
    answers = {}
    del question_dict[('00', 'rep-gr')]
    for question in question_dict:
        if question[0] in ["06", "14", "15"]:
            ask = cond_ask(question[0], answers)
        else: ask = True
        if ask:
            try:
                answers.update({question:input(question_dict[question])})
            except:
                print("Je n'ai pas pu récuperer la réponse à cette question, mais tant pis je passe à la suite")
    return answers
'''
def ask(nom_question,question,dico):
	"""asks question and puts it the answer a dico"""
	#system(question)
	answer = ask_listen_main(question)
	dico.update({nom_question:answer})
'''

def chat(num_ligne, question_dict):
   #answers = {"name":name}#pour stocker les réponses
    answers = {}
    name = sheet.cell(num_ligne, 2).value + ' ' + sheet.cell(num_ligne, 4).value
    greeting = "Bonjour {}, je suis Boris, un robot developpé par des étudiants pour récolter de l'information. Est-ce que vous auriez 5 minutes pour répondre à quelques questions concernant votre situation professionnel?\nVeuillez repondre clairement par oui ou non\n".format(name)
    oui = ['oui','oui oui', 'ok', 'pas de souci', 'okay',"d'accord","ouais","wesh","bon d'accord",'alrighty then', 'yes']
    non = ['non', 'non non','pas disponible','je suis occupé','etc', 'pas du tout','get lost', 'désolée','non désolée',"j'ai pas le temps", "pas vraiment","pas trop", "pas beacoup","absolument pas",'nope']
	#### demander la personne si elle veut continuer ####
    rep_gr = ask_listen_begin(greeting)
    
    if rep_gr.lower() not in oui and rep_gr.lower() not in non or rep_gr == None: 
		#system("say Je n'ai pas compris, Je vais répéter la question")
        rep_gr = ask_listen_begin("Avez vous 5 minutes a m\'accorder ?")# ask again
    if rep_gr.lower() in non:
		#system("say Cela ne prendra seulement 5 minutes! Voulez-vous continuer?")
        rep_gr = ask_listen_begin("Cela ne prendra que 5 minutes! Voulez-vous continuer?")
        if rep_gr.lower() not in oui and rep_gr.lower() not in non:
            tospeak("Je suis désolé, je n'ai toujours pas compris. Je vais raccorcher, je tenterai de vous rappeler a un autre moment. Merci.")
            system("Je suis désolé, je n'ai toujours pas compris. Je vais raccorcher, je tenterai de vous rappeler a un autre moment. Merci.")
            answers.update({('00', 'rep-gr'):False})
            return answers ### EN CAS D'INCOMPREHENSION : RETOUR FALSE ###
        if rep_gr.lower() in non:
            print("ok bye")
            tospeak("J\'ai pris note de votre refus, passez une bonne journée, au revoir!")
            system("say J'ai pris note de votre refus, passez une bonne journée, au revoir!")
            answers.update({('00', 'rep-gr'):"non"}) 
            return answers ### EN CAS DE REFUS : RETOUR NON ###
			#hang up?
    if rep_gr.lower() in oui:
        tospeak("Merci, commençons")
        system("say Merci, commençons")
        answers.update({"reponse_greeting":"oui"})
	
    ####Debut des questions
    
	####### GRAND BOUCLE ###########
    answers = get_answers(question_dict.copy())
	################################

    tospeak('Merci beaucoup de nous avoir accordé votre temps! Bonne journée et bonne continuation!')
    system('say Merci beaucoup de nous avoir accordé votre temps! Bonne journée et bonne continuation!')
    print(answers)
    return answers

def fill_gs(num_row, index_first_ans, dic_answers):
    if (('00', 'rep-gr') in dic_answers):
        sheet.update_cell(num_row, index_first_ans, dic_answers.get(('00', 'rep-gr')))
    else:
        sheet.update_cell(num_row, index_first_ans, "oui")
        for i in range(len(dic_answers)):
            sheet.update_cell(num_row, i+index_first_ans+1, list(dic_answers.values())[i])
    sheet.update_cell(num_row, 1, str(datetime.datetime.now()))
    
def delete_gs_answers(num_row, index_first_ans, dic_answers):
    for i in range(len(dic_answers)+1):
        sheet.update_cell(num_row, i+index_first_ans, "")
    sheet.update_cell(num_row, 1, "")

#%%
ans = {}
max_num_ligne = len(sheet.col_values(2))

num_ligne = input("Saisissez un numero de ligne supérieur a 1 :\n")
try:
    num_ligne = int(num_ligne)
    assert num_ligne > 1
    assert num_ligne < max_num_ligne+1
    ans = chat(num_ligne, dico_questions)
except ValueError:
    print("Vous n'avez pas saisi un nombre.")
except AssertionError:
    print("Error: Le numero de ligne saisi est inférieure à 2 ou supérieur à {}.".format(max_num_ligne))

fill_gs(num_ligne, index_00, ans)
delete_gs_answers(num_ligne, index_00, ans)


