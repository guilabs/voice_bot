import gspread
from oauth2client.service_account import ServiceAccountCredentials


# use creds to create a client to interact with the Google Drive API
scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', scope)
client = gspread.authorize(creds)


client.list_spreadsheet_files()
for s in client.openall():
    print (s.title)
# Find a workbook by name and open the first sheet
# Make sure you use the right name here.
sheet = client.open("Dataset - Voicebot").sheet1

# Extract and print all of the values
list_of_hashes = sheet.get_all_records()
print(list_of_hashes)

sheet.get_all_values()

sheet.row_values(2) # To show row 2
sheet.col_values(2) # To show col 2
sheet.cell(2, 2).value # To show cell 2, 2
sheet.update_cell(2, 1, "TestWriting") # To modify cell 2, 1
# To insert a row
row = ["17/5/19","Philippe","","0000000000","","Apprenant","S","","OK"]
index = 25
sheet.insert_row(row, index)
sheet.delete_row(25) # To delete a row
sheet.row_count # To count the number of rows BUT not sure cause empty rows !
# Test
# To insert in google sheet
Q = sheet.row_values(1)
del Q[0:7]

sheet.update_cell(2, 1, "TestWriting")
j = 104
for i in range(len(Q)):
    sheet.update_cell(2, i+8, chr(j))
    j += 1
# To delete the entries
sheet.update_cell(2, 1, "")
for i in range(len(Q)):
    sheet.update_cell(2, i+8, "")
