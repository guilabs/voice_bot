#################################################################
#CODE POUR RECUPERER UNE DATE ET HEURE DE RDV DE L'INTERLOCUTEUR#
#################################################################

import time
import calendar
import timestring #converts string to datetime (english only)
from translate import Translator #translates (so we can use timestring in french)
from datetime import timedelta, datetime
import datetime
translator=Translator(to_lang="en",from_lang='fr')

#demo of the translator 
t = 'le 27 mars 2014 à 18 heures'
t2= translator.translate(t) #=March 27, 2014 at 6 pm
#print(t2) 

#demo of timestring - converts 'March 27, 2014 at 6 pm' to timestring 
#print(timestring.Date(t2))

#some basic values that will be usefull for the final function 
today = datetime.datetime.today()
tomorrow = datetime.date.today() + datetime.timedelta(days=1)
ce_soir = timestring.Date(str(today)+' 18:00:00')
#print(today)

#extracting the year/month/day etc
t3 = timestring.Date(t2)
#print(t3.year)
#print(t3.month)
#print(t3.day)
#print(t3.hour)

#converting timestring to datetime format
t4 = datetime.datetime(t3.year,t3.month,t3.day,t3.hour)

def ask_time():
    #translates the answer into english and then converts it into datetime inshalla 
    #returns datetime of rdv to put in the calendar/ sheet
    #RETURNS NONE IF NO DATE FOUND
	dt = ""
	p,j = False,0
	#>>>>>>BOUCLE
	while(p==False and j<3):
		answer = input("Quel jour et à quelle heure puis-je vous rappeler? : ")
		print('merci, je traite votre demande')
		trans_ans = translator.translate(answer)#translate 
		print(trans_ans)
		try:
			dt = timestring.Date(trans_ans)
			p=True
		except:
			print("Je suis désolée, je n'ai pas compris une date")
		j+=1
	#>>>>>>BOUCLE /fin 

	if p==False:
		#print("vous etes dans la fonction ask_time")
		print("N'ayant pas pu obtenir une réponse pour la prise de rendez-vous, je vais racrocher.")
		print("Si vous souhaitez nous rappeler par rapport à nos offres de travail, vous pouvez nous joindre à 06...")
		print("Bonne journée et bonne continuation")
		return None #RETURNS NONE IF NO DATE FOUND = PROBABLY MEANS THE INTERLOCUTER DOESN'T WANT A RDV
    
    #to be able to manipulate 'dt', the type must be datetime not timestring >>>
	dt = datetime.datetime(dt.year,dt.month,dt.day,dt.hour,dt.minute)#convert to datetime

    #########################Traitement de cas specaiaux########################
	today = datetime.datetime.today()
	if 'prochain' in answer or dt<today:
		#the problem being that if you say 'next thursday' it will return thursday of this week, even if it's friday!!!
		#so let's try and fix this by adding 7 days 
		w = datetime.timedelta(days=7)
		dt = dt+w
	if 'soir' in answer:
		dt = dt.replace(hour=18)
	if 'midi' in answer:
		dt = dt.replace(hour=12)
	if 'weekend' in answer:
        #si qqn dit 'weekend' on met par défaut le dimanche (à la même heure?)
		today = datetime.datetime.today()
        #d = datetime.strptime('2013-05-27', '%Y-%m-%d') # Monday
		t = timedelta((13 - today.weekday()) % 7) #13 = dimanche , 12 = samedi
		dt = today + t
        #on reconverti en datetime
		dt = datetime.datetime(dt.year,dt.month,dt.day,dt.hour,dt.minute)
	if 'après demain' in answer or 'après-demain' in answer:
		x = datetime.timedelta(days=1)
		dt=dt + x
	if 'hier' in answer:
		print('arretes de déconner wesh')
	if dt.hour == 0:
        #si on dit un jour seulement, par défaut il retourne minuit, donc on change à 15h
        #pour ne pas appeler à minuit quoi 
		dt = dt.replace(hour=15)
	return dt

#main function
def time_main():
	"""cette fonction demande à l'utilisateur s'il souhaite prendre rdv
	si la réponse est oui il récupère la date souaitée, demnande la confirmation et puis le retourne
	il retourne None si l'utilisateur ne veut pas de rendez-vous ou si l'ordi ne comprends pas l'user plus que 3 fois"""
	oui = ['oui','oui oui', 'ok', 'pas de souci', 'okay',"d'accord","ouais","wesh","bon d'accord",'alrighty then', 'yes', 'oki']
	non = ['non', 'non non', "no", 'pas disponible','je suis occupé','etc', 'pas du tout','get lost', 'désolée','non désolée',"j'ai pas le temps", "pas vraiment","pas trop", "pas beacoup","absolument pas",'nope','pas envie']
	q,i = False,0
	while (q==False and i<3):
		rep = input("Seriez-vous d'accord qu'on vous rappelle à une heure qui vous convient? : ")
		if rep in oui:
			q=True
			print("Très bien.")
		elif rep in non:
			print("D'accord je vous souhaite une bonne journée, au revoir")
			return None
		else:
			print("Je n'ai pas compris votre réponse, je repete")
			i+=1
	if (i==3 and q==False):
		print("Je suis désolée je n'ai toujours pas compris votre réponse, tant pis, je vous souhaite une bonne journée, au revoir")
		return None 
	#he has to try 3 times to take a rdv but if the person doesn't confirm 3 times, then hang up !	
	k=0
	l=0
	while(k<3):
		date = ask_time()
		if date != None:
			while(l<3):
				print("Vous avez choisi le {} {} à {} heures {}".format(date.day,date.month,date.hour,date.minute), " Voulez-vous confirmer ce rendez-vous? : ")
				r=input()
				if r in oui:
					print("Merci, je vous rappellerai donc le {} {} à {} heures {} . A bientôt.".format(date.day,date.month,date.hour,date.minute))
					return date
				elif r in non:
					print("Vous ne confirmez pas donc je vais redemander")
					break
				elif r not in oui and r not in non:
					print("Je n'ai pas compris, pardon.")
				l+=1
		else:
			return None 
		if l==3:
			print("ah je suis désolée, je n'arrive pas à compris votre réponse pour la confirmation")
			#demander à boris quoi faire dans ce cas
			break
		k+=1
	print("N'ayant pas pu obtenir une réponse pour la prise de rendez-vous, je vais racrocher.")
	print("Si vous souhaitez nous rappeler par rapport à nos offres de travail, vous pouvez nous joindre à 06...")
	print("Bonne journée et bonne continuation")
	return None


a1= time_main()
print(a1)
