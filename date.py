### OLD VERSION ###

#################################################################
#CODE POUR RECUPERER UNE DATE ET HEURE DE RDV DE L'INTERLOCUTEUR#
#################################################################

import time
import calendar
import timestring #converts string to datetime (english only)
from translate import Translator #translates 
from datetime import timedelta, datetime
import datetime
translator=Translator(to_lang="en",from_lang='fr')

#demo of the translator 
t = 'le 27 mars 2014 à 18 heures'
t2= translator.translate(t)
print(t2)

#demo of timestring - converts 'March 27, 2014 at 6 pm' to timestring 
print(timestring.Date(t2))

#some basic values that will be usefull for the final function 
today = datetime.datetime.today()
tomorrow = datetime.date.today() + datetime.timedelta(days=1)
ce_soir = timestring.Date(str(today)+' 18:00:00')
#print(today)

#extracting the year/month/day etc
t3 = timestring.Date(t2)
print(t3.year)
print(t3.month)
print(t3.day)
print(t3.hour)

#converting timestring to datetime format
t4 = datetime.datetime(t3.year,t3.month,t3.day,t3.hour)

#main function
def ask_time():
	"""cette fonction demande à l'utilisateur s'il souhaite prendre rdv
	si la réponse est oui il récupère la date souaitée et le retourne"""

    #translates the answer into english and then converts it into datetime inshalla 
    #returns datetime of rdv to put in the calendar/ sheet
    #RETURNS NONE IF NO DATE FOUND
	oui = ['oui','oui oui', 'ok', 'pas de souci', 'okay',"d'accord","ouais","wesh","bon d'accord",'alrighty then', 'yes', 'oki']
	non = ['non', 'non non','pas disponible','je suis occupé','etc', 'pas du tout','get lost', 'désolée','non désolée',"j'ai pas le temps", "pas vraiment","pas trop", "pas beacoup","absolument pas",'nope']
	p,i = False,0
	while (p==False and i<3):
		rep = input("Seriez-vous d'accord qu'on vous rappelle à une heure qui vous convient?")
		if rep in oui:
			p=True
			print("Très bien.")
		elif rep in non:
			print("D'accord je vous souhaite une bonne journée, au revoir")
			return None
		else:
			print("je n'ai pas compris votre réponse, je repete")
			i+=1
	if (i==3 and p==False):
		print("Je suis désolée je n'ai toujours pas compris votre réponse, tant pis, je vous souhaite une bonne journée, au revoir")
		return None 
	dt = ""
	p,j = False,0
	#>>>>>>BOUCLE
	while(p==False and j<3):
		answer = input("Quel jour et à quelle heure puis-je vous rappeler? : ")
		print('merci, je traite votre demande')
		trans_ans = translator.translate(answer)#translate 
		print(trans_ans)
		try:
			dt = timestring.Date(trans_ans)
			p=True
		except:
			print("Je suis désolée, je n'ai pas compris une date")
		j+=1
	#>>>>>>BOUCLE /fin 

	if p==False:
		print("désolée je n'ai pas compris cette date")
		return None #RETURNS NONE IF NO DATE FOUND = PROBABLY MEANS THE INTERLOCUTER DOESN'T WANT A RDV
    
    #to be able to manipulate 'dt', the type must be datetime not timestring >>>
	dt = datetime.datetime(dt.year,dt.month,dt.day,dt.hour,dt.minute)#convert to datetime
    #########################Traitement de cas specaiaux########################
	if 'prochain' in answer:
		#the problem being that if you say 'next thursday' it will return thursday of this week
		#so let's try and fix this by adding 7 days 
		w = datetime.timedelta(days=7)
		dt = dt+w
	if 'weekend' in answer:
        #si qqn dit 'weekend' on met par défaut le dimanche (à la même heure?)
		today = datetime.datetime.today()
        #d = datetime.strptime('2013-05-27', '%Y-%m-%d') # Monday
		t = timedelta((13 - today.weekday()) % 7) #13 = dimanche , 12 = samedi
		dt = today + t
        #on reconverti en datetime
		dt = datetime.datetime(dt.year,dt.month,dt.day,dt.hour,dt.minute)
		print(dt.hour)
	if 'après demain' in answer or 'après-demain' in answer:
		print('après-demain')
		x = datetime.timedelta(days=1)
		dt=dt + x
	if 'hier' in answer:
		print('arretes de déconner wesh')
	if dt.hour == 0:
        #si on dit un jour seulement, par défaut il retourne minuit, donc on change à 15h
        #pour ne pas appeler à minuit quoi 
		dt = dt.replace(hour=15)
	print("merci vous avez choisi le {} {} à {} heures, à bientot!".format(dt.day,dt.month,dt.hour))
	return dt

a1= ask_time()
print(a1)
