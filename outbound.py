from twilio.rest import Client
from twilio.twiml.voice_response import VoiceResponse, Gather, Say

# Your Account Sid and Auth Token from twilio.com/console
# DANGER! This is insecure. See http://twil.io/secure
account_sid = 'yours'
auth_token = 'yours'
client = Client(account_sid, auth_token)

call = client.calls.create(
                        url='yours/answer',
                        to='+yours',
                        from_='+yours'
                    )

print(call.sid)