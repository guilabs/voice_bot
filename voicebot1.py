import pandas as pd
import speech_recognition as sr  
from os import system

#le client peut ajouter plus de questions dans ce dico
q = {"secteur":"say Dans quel secteur évoluez-vous?",
"exp":"say Quel est votre nombre d’année d’expérience ?",
"competences":"say Veuillez citer 3 de vos compétences. Par exemple: Java, Python, Gestion de projet",
"structure":"say Dans quel type de structure préférez-vous travailler ? Veuillez choisir entre: Petite,  Grande, ou Indifférente",
"ville":"say Dans quelle ville êtes-vous?",
"lieu":"say Quelles sont vos préférences de lieu de travail ? Veuillez choisir entre: Sur site , à distance, ou Indifférent",
"mobile":"say Etes-vous mobile ? Veuillez choisir entre: Ville, Pays, ou Internationale",
}

def ask_listen(message):
	#can be used to replace input() but gets answers from microphone instead of from the keyboard
	# eg. answer = ask_listen('say your answer')
	answer = ""
	r = sr.Recognizer()  
	print(message)
	with sr.Microphone() as source:
		r.adjust_for_ambient_noise(source)
	    #r.dynamic_energy_threshold = False
		audio = r.listen(source)
		try:
			answer = r.recognize_google(audio, language='fr-FR')
			print("Vous avez dit : {}".format(answer))
		except:
			system("say Désolée je n'ai pas entendu")
	return answer

def get_answers(question_dict):
	"""returns a dico with key:name of the question, value:answer to the question"""
	answers = {}
	for question in question_dict:
		#print(question, "corresponds to", question_dict[question])
		try:
			system(question_dict[question])
			answers.update({question:ask_listen(question)})
		except:
			print("Je n'ai pas pu récuperer la réponse à cette question, mais tant pis je passe à la suite")
	return answers

def ask(nom_question,question,dico):
	"""asks question and puts it the answer a dico"""
	system(question)
	answer = ask_listen(question)
	dico.update({nom_question:answer})


def chat(name,question_list):
	"""main chat bot function, returns dico with questions and answers
	if answer not heard > empty string"""
	system("say Bonjour, je suis un robot qui travaille pour un cabinet de statistique dans le domaine de l’emploi. Est-ce que vous auriez 5 minutes pour répondre à quelques questions concernant votre situation professionnel? Veuillez repondre clairement par oui ou non")		
	answers = {"name":name}#pour stocker les réponses 
	greeting = "Bonjour {}, je suis Boris, un bot developpé par des étudiants pour récolter de l'information. Est-ce que vous auriez 5 minutes pour répondre à quelques questions concernant votre situation professionnel?\nVeuillez repondre clairement par oui ou non\n".format(name)
	oui = ['oui','oui oui', 'ok', 'pas de souci', 'okay',"d'accord","ouais","wesh","bon d'accord",'alrighty then', 'yes']
	non = ['non', 'non non','pas disponible','je suis occupé','etc', 'pas du tout','get lost', 'désolée','non désolée',"j'ai pas le temps", "pas vraiment","pas trop", "pas beacoup","absolument pas",'nope']
	#### demander la personne si elle veut continuer ####
	rep_gr = ask_listen(greeting)
	print(rep_gr)
    #if detect_repeat(rep_gr, greeting):
        #rep_gr = input(greeting)
	if rep_gr.lower() not in oui and rep_gr.lower() not in non or rep_gr == None: 
		system("say Je n'ai pas compris, Je vais répéter la question")
		rep_gr = ask_listen(greeting)# ask again
	if rep_gr.lower() in non:
		system("say Cela ne prendra seulement 5 minutes! Voulez-vous continuer?")
		rep_gr = ask_listen("Cela ne prendra seulement 5 minutes! Voulez-vous continuer?")
		if rep_gr.lower() not in oui and rep_gr.lower() not in non:
			system("say Je suis désolé, je n'ai toujours pas compris. Je vais continuer avec les questions, mais si vous ne souhaitez pas répondre, veuillez racrocher. Merci.")
		if rep_gr.lower() in non:
			print("ok bye")
			system("say D'accord, passez une bonne journée, au revoir!")
			return None
			#hang up?
	if rep_gr.lower() in oui:
		system("say Merci, commençons")
	####Debut des questions
	ask("statut","say Quel est votre statut actuel Veuillez choisir entre: CDI , CDD , Freelance , Stagiaire , Alternant",answers)
	##########------##########
	ask("correspond","say Est-ce que votre poste actuel vous correspond?",answers)
	#########------##########
	if answers["correspond"] in non:
		ask("poste_souhaite","say Sur quel type de poste souhaitez-vous évoluer?",answers)

	####### GRAND BOUCLE ###########
	answers.update(get_answers(question_list))#demande les questions communs et les mettre dans le dico answer
	################################

	if answers["statut"] == "freelance":
		ask("TJM",'say Quel est votre Taux journalier moyen?',answers)
	else:
		ask("salaire",'say Quelles sont vos prétentions salariales?',answers)
	##bye bye	
	system('say Merci beaucoup de nous avoir accordé votre temps! Bonne journée et bonne continuation!')
	print(answers)
	return answers

name = input("Entrez votre nom: ")
chat(name, q)